function dhdt=fEx2Es10a(t,h)

global Fin A hss2 tnewhss Kc Foutss

if t<tnewhss
    % stato stazionario
    Fout=Foutss;
else
    % nuovo setpoint (problema di servomeccanismo)
    epsilon=hss2-h;
    Fout=Foutss+Kc*epsilon;
    
end
dhdt=(Fin-Fout)/A;
end