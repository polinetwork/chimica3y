function [Z,A,B,S,k]=EoS(T,P,Tc,Pc,om,tipo)

%EoS calcola Z con diverse EoS: tipo 1 vdW
%                               tipo 2 RK        
%                               tipo 3 RKS            
%                               tipo 4 PR    
% restituisce [Z,A,B,S,k]

R=8.314;
RT=R*T;
RTc=R*Tc;
S=0;
k=1;

%parametri dell'equazione nella forma Z^3+alfa*Z^2+beta*Z+gamma=0
if tipo==1      %vdW
    a=27*RTc^2/(64*Pc);
    b=RTc/(8*Pc);
    A=a*P/(RT)^2;
    B=b*P/(RT);
    alfa=-1-B;
    beta=A;
    gamma=-A*B;
end
if tipo==2      %RK
    TR=T/Tc;
    a=0.42748*RTc^2/(Pc*sqrt(TR));
    b=0.08664*RTc/Pc;
    A=a*P/(RT)^2;
    B=b*P/(RT);
    alfa=-1;
    beta=A-B-B^2;
    gamma=-A*B;
end
if tipo==3      %RKS
    S=0.48+1.574*om-0.176*om^2;
    k=(1+S*(1-sqrt(T/Tc)))^2;
    a=0.42748*k*RTc^2/Pc;
    b=0.08664*RTc/Pc;
    A=a*P/(RT)^2;
    B=b*P/(RT);
    alfa=-1;
    beta=A-B-B^2;
    gamma=-A*B;
end
if tipo==4      %PR
    S=0.37464+1.54226*om-0.26992*om^2;
    k=(1+S*(1-sqrt(T/Tc)))^2;
    a=0.45724*k*RTc^2/Pc;
    b=0.07780*RTc/Pc;
    A=a*P/(RT)^2;
    B=b*P/(RT);
    alfa=-1+B;
    beta=A-2*B-3*B^2;
    gamma=-A*B+B^2+B^3;
end

%risoluzione numerica cubica
coeff=[1 alfa beta gamma];
Z=roots(coeff);     %calcola le radici del polinomio
Z=sort(Z);          %ordina i valori
