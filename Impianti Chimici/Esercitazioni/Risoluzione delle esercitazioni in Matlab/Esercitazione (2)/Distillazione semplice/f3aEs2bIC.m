function F=f3aEs2bIC(X)

global z k

F=sum(z.*(k-1)./(1+X.*(k-1)));