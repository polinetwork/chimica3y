function F=fEs1Es8PdIC(X)

global DP L G0 Ds ro mu

F=DP/L-(((1-X)/(X^3))*G0^2/(Ds*ro))*((150*mu/(Ds*G0))*(1-X)+1.75);