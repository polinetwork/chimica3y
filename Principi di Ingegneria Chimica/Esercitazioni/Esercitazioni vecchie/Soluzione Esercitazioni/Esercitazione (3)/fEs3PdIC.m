function F=fEs3PdIC(X)

global taug rog mug Dint

Reg=rog*X*Dint/mug;
f=16/Reg+0.079*(Reg^(-0.25));
F=taug-(f*rog*X^2)/2;