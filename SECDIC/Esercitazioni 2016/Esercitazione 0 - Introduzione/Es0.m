function main
    clear all
    clc
    global a b c

    a=3; b=4; c=2;
    [t,x]=ode45(@fdiff,[0 5],0);
    plot(t,x)
    grid on    
end

function f=fdiff(t,x)
    global a b c
    
    f=a*exp(-x)-b*x+c;
end