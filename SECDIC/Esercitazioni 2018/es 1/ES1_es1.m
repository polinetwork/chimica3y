h = 0.1; %passo
t = 0:h:20; %vettore dei tempi
nstep = length(t); %numero step
y = zeros(1,nstep); %vettore y vuoto
y(1) = 5; %condizione iniziale

%N.B. --> OPPURE il contatore va da 1 a nstep-1 e faccio y(i+1) = ... 

for i = 2:nstep
    y(i) = y(i-1) + 0.1*(-1*y(i-1)); % <-- N.B. la funzione � scritta direttamente qui, se no potevo anche fare una function a parte, da chiamare
end

hold on

plot(t,y)

%Poi posso fare un controllo con la soluzione analitica [slide]...