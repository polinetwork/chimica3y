clear all
clc
close all

global A1 H1 Ad1 A2 H2 Ad2 cd Fin g

D1=0.5;%m
A1=pi/4*D1^2;
H1=1.5;
d1=0.1;
Ad1=pi/4*d1^2;
h1o=0.7;

D2=0.5;%m
A2=pi/4*D2^2;
H2=1.5;
d2=0.1;
Ad2=pi/4*d2^2;
h2o=0.7;

Fin=0.04; %m3/s
cd=0.61;
g=9.80665;%m/s2

options=odeset('AbsTol',1e-12,'RelTol',1e-8,'OutputFcn',@fun);
[t,y]=ode45('ODEserbatoi',[0 500],[h1o h2o],options);
h1=y(:,1);
h2=y(:,2);
plot(t,h1,t,h2,'r')
grid on



