% Risoluzione della seconda parte dell'esercitazione 11 di I.C.

clear all

global Dis L Ais hi Ai st lambda ro g beta nu Amlt Tp Ta Tv Pra

%% Dati:
Di=0.154; %[m]
De=0.168; %[m]
Tp=323.15; %[K]
Ta=294.15; %[K]
PH2O=28*1e5; %[Pa]
v=5; %[m/s]
L=1;
g=9.81;

% Si consideri il vettore delle specie S=[aria vapore metalli coibente]', 0
% sta ad indicare che non si conoscono i dati:
nu=[1.967e-5 2.0413e-5 0 0]';
cp=[1007 2420 0 0]';
lambda=[0.0282 0.044 43.275 0.069]';
ro=[1.09 10.8445 0 0]';

% Tensione di vapore di H2O nella forma lnPv=A-B/(T+C), con T in [K] e Pv
% in [mmHg]
A=18.3036;
B=3816.44;
C=-46.13;

%% Risoluzione dell'esercitazione:


disp('----- Spessore di isolante incognito e Tp nota -----')

Tv=(B/(A-log(PH2O/101325*760)))-C+85;
Rev=ro(2)*Di*v/nu(2);
Prv=nu(2)*cp(2)/lambda(2);
Nui=0.023*Rev^(0.8)*Prv^(0.3);
hi=Nui*lambda(2)/Di;
st=(De-Di)/2;
Ai=pi*Di*L;
Ae=pi*De*L;
Amlt=(Ae-Ai)/log(Ae/Ai);
Dis=De;
Ais=pi*Dis*L;
Tfilm=(Tp+Ta)/2;
beta=1/Tfilm;
Pra=nu(1)*cp(1)/lambda(1);
X0=0.08;
X=fzero('fEs11bIC',X0);

disp(['Il valore assunto dallo spessore di isolante � pari a ',num2str(X),' [m]'])

si=X;
Des=Dis+2*si;
Aes=pi*Des*L;
Amls=(Aes-Ais)/log(Aes/Ais);
U=(Aes/(hi*Ai)+st/lambda(3)*Aes/Amlt+si/lambda(4)*Aes/Amls)^(-1);
Gr=Des^3*ro(1)^2*g*beta*(Tp-Ta)/(nu(1)^2);
Nue=0.525*(Gr*Pra)^0.25;
he=Nue*lambda(1)/Des;

Utot=(1/U+1/he)^(-1);
dispersioni=Utot*Aes*(Tv-Ta);

disp(['Il valore assunto dalle dispersioni � pari a ',num2str(dispersioni),' [W]'])


