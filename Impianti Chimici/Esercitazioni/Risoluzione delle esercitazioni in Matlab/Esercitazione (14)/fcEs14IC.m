function F=fcEs14IC(fc)

global Rec3

F=1/sqrt(fc)-4*log10(Rec3*sqrt(fc))+0.4;