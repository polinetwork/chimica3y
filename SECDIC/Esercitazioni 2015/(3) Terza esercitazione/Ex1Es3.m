%% Esercitazione (3) di SECDIC

clc
close all
clear all

global Fin Fout Abase H

% Dati:

D=6; % [m]
H=4; % [m] 
Fin=10; % [m^3/min] 
Fout=10; % [m^3/min] 
h0=0.5; % [m]

% Risoluzione:

% Primo punto

Abase=pi*D^2/4;

options= odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.001:20; % [min]
[t,h] = ode45('fEx1Es3',tSpan,h0,options);

figure('Name','Andamento di h nel caso Fin=10 e Fout=10','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [min]')
ylabel('Altezza nel serbatoio [h]')
title('Andamento di h con il tempo')

% Secondo punto

Fout=0; % [m^3/min] 

% Prima alternativa

[t,h] = ode45('f2Ex1Es3',tSpan,h0,options);

figure('Name','Andamento di h nel caso Fin=10 e Fout=0: modifica della function','NumberTitle','off')
plot(t,h)
legend('h')
grid on
xlabel('Tempo [min]')
ylabel('Altezza nel serbatoio [h]')
axis([0 20 0.5 5])
title('Andamento di h con il tempo')

% Seconda alternativa

[t,h] = ode45('fEx1Es3',tSpan,h0,options);


for i=1:length(h)
    if h(i)<4
        h(i)=h(i);
    else
        h(i)=4;
    end
end

figure('Name','Andamento di h nel caso Fin=10 e Fout=0: ciclo for','NumberTitle','off')
plot(t,h)
legend('h')
grid on
xlabel('Tempo [min]')
ylabel('Altezza nel serbatoio [h]')
axis([0 20 0.5 5])
title('Andamento di h con il tempo')

% Terzo punto

Fin=5; % [m^3/min] 
Fout=10; % [m^3/min] 

% Prima alternativa:

[t,h] = ode45('fEx1Es3',tSpan,h0,options);

for i=1:length(h)
    if h(i)<0
       h(i)=0;
    end
end

figure('Name','Andamento di h nel caso Fin=5 e Fout=10: ciclo for','NumberTitle','off')
plot(t,h)
legend('h')
grid on
xlabel('Tempo [min]')
ylabel('Altezza nel serbatoio [h]')
axis([0 6 -5 5])
title('Andamento di h con il tempo')

% Seconda alternativa

[t,h] = ode45('f3Ex1Es3',tSpan,h0,options);


figure('Name','Andamento di h nel caso Fin=5 e Fout=10: modifica della function','NumberTitle','off')
plot(t,h)
legend('h')
grid on
xlabel('Tempo [min]')
ylabel('Altezza nel serbatoio [h]')
axis([0 6 -5 5])
title('Andamento di h con il tempo')

% Quarto punto

D=22; % [m]
H=8; % [m] 
Fin=5; % [m^3/min] 
Fout=10; % [m^3/min] 
h0=0.5; % [m]

Abase=pi*D^2/4;

options= odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.01:40; % [min]
[t,h] = ode45('fEx1Es3',tSpan,h0,options);

for i=1:length(h)
    if h(i)<0
       h(i)=0;
    end
end

figure('Name','Andamento di h nel caso Fin=5 e Fout=10, con H=8 e D=22: ciclo for','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [min]')
ylabel('Altezza nel serbatoio [h]')
axis([0 40 -1 1])
title('Andamento di h con il tempo')