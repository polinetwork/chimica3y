% Risoluzione dell'esercitazione (8) di PdIC

clc
clear all 
close all

%% Esercizio (1)

global DP L G0 Ds ro mu

% Dati:
S=9.42e-2;
L=1.8254;
Ds=2e-3;
DP=1088230.5;
m=1.84167;
mu=5.65e-2;
ro=1286.5;
G0=m/S;

X0=0.4;

X=fzero('fEs1Es8PdIC',X0);

disp(['Il valore assunto dalla frazione di vuoto del letto catalitico � pari a ', num2str(X)])

