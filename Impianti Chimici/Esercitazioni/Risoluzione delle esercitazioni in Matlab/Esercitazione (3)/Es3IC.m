%% Risoluzione dell'esercitazione n.(3) di Impianti Chimici: Distillazione Binaria (Mattia Usuelli)

clc
clear all
close all

global A B C P xb xt z xD xB alfa

%% Dati: Si riferiscono alle specie S=[benzene toluene]
q=1; % Dal momento che la miscela entrante � liquida al punto di bolla, il fattore entalpico vale 1
z=[0.3 0.7];
P=1; %[atm]
A=[15.9008 16.0137]; % Parametri della Antoine
B=[2788.51 3096.52];
C=[-52.36 -53.67];
Tebn=[353.3 383.8]; %[K]

%% Risoluzione:

% Punto 1)
disp('------ Punto 1) ------')
% Costruzione dei diagrammi delle temperature di bolla e di
% rugiada in funzione della frazione molare del composto pi� leggero, 
% a P = 1 [atm]
disp('Costuzione dei diagrammi delle temperature di bolla e di rugiada in funzione della frazione molare del composto pi� leggero, a P = 1 [atm]')
xb=0:0.001:1;
xt=1-xb;
T0=Tebn(2)+xb.*(Tebn(1)-Tebn(2)); % Valore lineare di primo tentativo
T=fsolve('f1Es3IC',T0);
Pev=exp(A(1)-B(1)./(T+C(1)))/760;
k=Pev./P;
yb=k.*xb;
subplot(1,2,1)
plot(xb,T,yb,T)
subplot(1,2,2)
plot(xb,xb,xb,yb)

% Punto 2)
disp('------ Punto 2) ------')
% Determinazione del numero di stadi teorici di una colonna di
% distillazione
disp('Caso a): Determinazione del numero di stadi teorici di una colonna di distillazione con il metodo di McCabe-Thiele')
xD=[0.95 0.05];
xB=[0.04 0.96];
% Determinazione del valore di alfa medio (volatilit� relativa) della
% colonna:
T0=500;
T=fzero('f2aEs3IC',T0);
alfafeed=exp(A(1)-B(1)/(T+C(1)))/(exp(A(2)-B(2)/(T+C(2))));
T=fzero('f2bEs3IC',T0);
alfatesta=exp(A(1)-B(1)/(T+C(1)))/(exp(A(2)-B(2)/(T+C(2))));
T=fzero('f2cEs3IC',T0);
alfabottom=exp(A(1)-B(1)/(T+C(1)))/(exp(A(2)-B(2)/(T+C(2))));
alfa=(alfafeed*alfatesta*alfabottom)^(1/3);
R0=2;
Rmin=fzero('f2dEs3IC',R0);
R=1.3*Rmin;
a=1/(alfa-1);
b=xD(1)/R+alfa*(1+R)/((1-alfa)*R);
c=xD(1)/(R*(alfa-1));
delta=[(-(a+b)+sqrt((a+b)^2-4*c))/2 (-(a+b)-sqrt((a+b)^2-4*c))/2];
Ns(1)=log((1/(xD(1)-delta(1))+1/(2*delta(1)+a+b))/(1/(z(1)-delta(1))+1/(2*delta(1)+a+b)))/log(-(a+delta(1))/(b+delta(1)));
Ns(2)=log((1/(xD(1)-delta(2))+1/(2*delta(2)+a+b))/(1/(z(1)-delta(2))+1/(2*delta(2)+a+b)))/log(-(a+delta(2))/(b+delta(2)));
Ns=Ns(1);
Ns=ceil(Ns);
disp(['Il numero di stadi del tronco superiore � pari a ',num2str(Ns)])
zmeno=(((1/(xD(1)-delta(1))+1/(2*delta(1)+a+b))/(-(a+delta(1))/(b+delta(1)))^Ns)-1/(2*delta(1)+a+b))^(-1)+delta(1);
BF=(z(1)-xD(1))/(xB(1)-xD(1));
DF=1-BF;
aprimo=1/(alfa-1);
bprimo=-((alfa-1)*BF*xB(1)+(R*DF+q-BF)*alfa)/((alfa-1)*(R*DF+q));
cprimo=(-BF*xB(1))/((R*DF+q)*(alfa-1));
deltaprimo=(-(aprimo+bprimo)+sqrt((aprimo+bprimo)^2-4*cprimo))/2;
Ninf=log((1/(zmeno-deltaprimo)+1/(2*deltaprimo+aprimo+bprimo))/(1/(xB(1)-deltaprimo)+1/(2*deltaprimo+aprimo+bprimo)))/log(-(aprimo+deltaprimo)/(bprimo+deltaprimo));
Ninf=ceil(Ninf);
disp(['Il numero di stadi del tronco inferiore � pari a ',num2str(Ninf)])
disp(['Il numero di stadi totali della colonna di distillazione � pari a ',num2str(Ninf+Ns)])
disp('Caso b): Determinazione del numero di stadi teorici di una colonna di distillazione con il metodo di Fenske-Gilliland')
Nmin=log(xD(1)/(1-xD(1))*(1-xB(1))/xB(1))/log(alfa);
FR=(R-Rmin)/(R+1);
disp('Ultilizzo della correlazione di Eduljee')
fiN=0.75-0.75*FR^0.5668;
N=(fiN+Nmin)/(1-fiN);
disp(['Il numero di stadi totali della colonna di distillazione � pari a ',num2str(ceil(N))])
disp('Ultilizzo della correlazione di Molokanov')
fiN=1-exp((1+54.4*FR)*(FR-1)/((11+117.2*FR)*sqrt(FR)));
N=(fiN+Nmin)/(1-fiN);
disp(['Il numero di stadi totali della colonna di distillazione � pari a ',num2str(ceil(N))])