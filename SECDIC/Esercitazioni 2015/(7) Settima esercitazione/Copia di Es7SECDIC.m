%% Risoluzione dell'esercitazione (7) di SECDIC
clc
clear all
close all

%% Risoluzione:

global ca0 ca1 ca2 ca3 tau k ca0pert

% Dati:
ca0=0.8; % [kmol/m^3]
ca1=0.4; % [kmol/m^3]
ca2=0.2; % [kmol/m^3]
ca3=0.1; % [kmol/m^3]

tau=2; % [min], corrisponde al rapporto tra V ed F
k=0.5; % [min^(-1)]

% Punto (1)

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.001:5; % [min]
[t,ca]=ode45('fEx1Es7',tSpan,ca0);

figure('Name','Andamento di ca','NumberTitle','off')
plot(t,ca,'-b')
grid on
legend('ca')
xlabel('Tempo [min]')
ylabel('Concentrazione di A [kmol/m^3]')
title('Andamento della ca con il tempo')

% Punto (2)
% Determiniamo come prima cosa gli stazionari dei 3 reattori:

% Due reattori in serie:
% Le condizioni al contorno sono determinate imponendo la stazionarietÓ dei
% vari bilanci inerenti ai singoli reattori (annullamento delle
% derivate)...
% castaz(i)=ca(i-1)/(1+k*tau) (dove ca(i-1) corrisponde alle condizioni al
% contorno espresse nel testo)
ca1staz=ca0/(1+k*tau);
ca2staz=ca1/(1+k*tau);
ca3staz=ca2/(1+k*tau);
ca0pert=1.8; % [kmol/m^3]
tSpan=0:0.001:15; % [min]
% Nella risoluzione delle equazioni differenziali imponiamo la
% perturbazione alla condizione iniziale:
Ciniz=[ca1staz ca2staz];
[t,ca]=ode45('fEx2Es7',tSpan,Ciniz);

figure('Name','Andamento di ca: due reattori in serie','NumberTitle','off')
plot(t,ca(:,1),'-b',t,ca(:,2),'-r')
grid on
legend('ca1','ca2')
xlabel('Tempo [min]')
ylabel('Concentrazione di A [kmol/m^3]')
title('Andamento della ca con il tempo')

% Punto (3)
% Determiniamo come prima cosa gli stazionari dei 3 reattori:

% Due reattori in serie:
tSpan=0:0.001:15; % [min]
Ciniz=[ca1staz ca2staz ca3staz];
[t,ca]=ode45('fEx3Es7',tSpan,Ciniz);

figure('Name','Andamento di ca: tre reattori in serie','NumberTitle','off')
plot(t,ca(:,1),'-b',t,ca(:,2),'-r',t,ca(:,3),'-m')
grid on
legend('ca1','ca2','ca3')
xlabel('Tempo [min]')
ylabel('Concentrazione di A [kmol/m^3]')
title('Andamento della ca con il tempo')