function f=sistema(t,X)

global P DHr v d cpi

F=[X(1); X(2); X(3); X(4)];
T=X(5);

K=exp(-4.186*(-22858+56.02*T)/(8.314*T));
C1=exp(3.49+4883*(1/T-1/506));
C2=exp(2.53-39060*(1/T-1/506));
C3=exp(1.54+8229*(1/T-1/506));
Pi=P*F/sum(F);

R=(Pi(1)*Pi(2)^2-Pi(3)/K)/(C1+C2*Pi(1)+C3*Pi(2))^2;

f=v'*R*d;
f(5)=-DHr*R*d/sum(F.*cpi');