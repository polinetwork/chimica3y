function f=ODE(t,h)

global A Ad Cd g Fino span H

if t<span
    Fin=Fino;
elseif t>=span && t<(2*span)
    Fin=0.67*Fino;
elseif t>=(2*span) && t<(3*span)
    Fin=Fino;
elseif t>=(3*span) && t<(4*span)
    Fin=0.5*Fino;
elseif t>=(4*span)
    Fin=Fino;    
end

if h<0
    h=0;
elseif h>H
    h=H;
end

f=(Fin-Cd*sqrt(2*g*h)*Ad)/A;