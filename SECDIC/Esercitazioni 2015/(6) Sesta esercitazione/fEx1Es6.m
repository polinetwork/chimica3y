function dhdt=fEx1Es6(t,h)

global Fin A epsilon

Fout=epsilon*sqrt(h);

dhdt=(Fin-Fout)/A;