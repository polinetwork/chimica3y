function F=f3b1Es2bIC(X)

global z k1

F=sum(z.*(k1-1)./(1+X.*(k1-1)));