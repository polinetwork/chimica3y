function f=ODE_es3(t,y)

global F1o F2 c1 c2 V 

c=y(1);

if t<50
  F1=F1o;
elseif t>=50  
  F1=F1o+0.04*(t-50);
end
F3=F1+F2;

f=(F1*c1+F2*c2-F3*c)/V;


end