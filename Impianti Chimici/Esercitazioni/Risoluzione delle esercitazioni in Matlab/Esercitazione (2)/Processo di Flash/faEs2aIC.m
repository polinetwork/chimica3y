function F=faEs2aIC(X)

global z k

F=sum(z.*(k-1)./(1+X.*(k-1)));