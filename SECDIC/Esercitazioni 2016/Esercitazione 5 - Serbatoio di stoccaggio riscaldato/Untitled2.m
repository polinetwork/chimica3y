clear all
clc

global H D Tin Te Qin ho ro cp Fin Fout U Qi i
i=1;

%problema2
D=8; %m
H=6; %m
Fin=10/60; %m3/s
Fout=10/60; %m3/s
Tin=60; %�C
Te=25; %�C
Qin=5000; %W
ho=2.5; %m
U=100; %W/m2K
ro=1000; %kg/m3
cp=4186; %J/kgK

options=odeset('AbsTol',1e-12, 'RelTol',1e-9,'OutputFcn',@fun);

%caso1: adiabatico
%ecc........

%caso2: non adiabatico
[t,y]=ode113('ODE',[0 60*60], [ho Tin],options);
h=y(:,1);
T=y(:,2);

figure(1)
plot(t,h,'r-o');
grid on
xlabel('t [s]')
ylabel('h [m]')
axis tight
figure(2)
plot(t,T,'g-o');
grid on
xlabel('t [s]')
ylabel('T [�C]')
axis([0 60*60 0 100])

%caso3 %mantenere costante T=60�C attraverso un Qi=-UA(Te-T)
figure(3)
plot(t,Qi)      %
grid on
xlabel('t [s]')
ylabel('Qi [W]')
