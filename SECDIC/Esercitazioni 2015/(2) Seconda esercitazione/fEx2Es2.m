function dTdt=fEx2Es2(t,T)
global F m Tin2 Q cp
dTdt=F/m*(Tin2-T)+Q/(m*cp);