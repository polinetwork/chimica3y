%% Primo esercizio dell'esercitazione (2) di SECDIC
clc
clear all
close all

global k1 k2 k3

% Dati:

B0=1; % [kmol/m^3]
S0=1; % [kmol/m^3]
k1=0.5; % [h^-1]
k2=1e-7; % [kmol/m^3]
k3=0.6;
c0=[B0 S0];
options= odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=[0 15]; % [h^-1]
[t,c] = ode23s('fEx1Es2',tSpan,c0,options);

plot(t,c(:,1),'-r',t,c(:,2),'-b')
legend('ca','cb')
xlabel('Tempo [h]')
ylabel('Concentrazioni delle specie, [mol/m^3]')
title('Andamento delle concentrazioni col tempo')