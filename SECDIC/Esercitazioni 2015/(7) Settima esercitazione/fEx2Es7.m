function dcadt=fEx2Es7(t,ca)

% ca(1)=ca1 ; ca(2)=ca2

global ca0pert tau k

dcadt(1)=(ca0pert-ca(1))/tau-k*ca(1);
dcadt(2)=(ca(1)-ca(2))/tau-k*ca(2);

dcadt=dcadt';