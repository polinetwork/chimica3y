%% Risoluzione dell'esercitazione n.(4) di Impianti Chimici: Distillazione multicomponente (Mattia Usuelli)

clc
clear all
close all

global z AntA AntB AntC xB P xD alfamedio q

%% Primo caso: componenti non distribuiti
disp('Primo caso: componenti non distribuiti')

% Dati del problema: si riferiscono al vettore S=[n-pentano n-esano
% n-eptano n-ottano];
q=1; % la miscela � liquida al punto di bolla, e di conseguenza il fattore entalpico � unitario
z=[0.04 0.40 0.50 0.06];
RRV=[1 0.98 0.01 0]; % Assumiamo che tutto l'n-pentano vada in testa, e che tutto l'n-ottano vada in coda
RRL=1-RRV;
P=1; % [atm]
F=100; % [mol/s]: � una mia assunzione
AntA=[15.8333 15.8366 15.8737 15.9426];
AntB=[2477.07 2697.55 2911.32 3120.29];
AntC=[-39.94 -48.78 -56.51 -63.63];
Tnb=[309.2 341.9 371.6 398.8]; % [K]
DHevTbn=[6160 6896 7576 8225]; % [cal/mol]
cpv=[31.02 38.86 45.15 52.23]; % [cal/mol K]
cpl=[37.614 51.70 56.12 63.89]; % [cal/mol K]
Tc=[469.6 507.4 540.2 568.8]; % [K]

% Risoluzione dell'esercitazione:
disp('------ Determinazione del numero di stadi ------')
DxD=RRV.*F.*z;
BxB=RRL.*F.*z;
D=sum(DxD);
B=sum(BxB);
xD=DxD./D;
xB=BxB./B;

% Determinazione della temperatura di bolla dell'alimentazione:
T0=300;
Tfeed=fzero('faEs4aIC',T0);

% Determinazione della temperatura di bolla del fondo colonna:
Tbottom=fzero('fbEs4aIC',T0);

% Determinazione della temperatura di rugiada di testa colonna:
Ttesta=fzero('fcEs4aIC',T0);

% Determinazione della volatilit� relativa dei diversi composti rispeto al
% n-eptano:
% Alimentazione:
alfafeed=(exp(AntA-AntB./(Tfeed+AntC)))./(exp(AntA(3)-AntB(3)/(Tfeed+AntC(3))));
% Testa:
alfatesta=(exp(AntA-AntB./(Ttesta+AntC)))./(exp(AntA(3)-AntB(3)/(Ttesta+AntC(3))));
% Fondo:
alfafondo=(exp(AntA-AntB./(Tbottom+AntC)))./(exp(AntA(3)-AntB(3)/(Tbottom+AntC(3))));

alfamedio=(alfafeed.*alfatesta.*alfafondo).^(1/3);
Nmin=log(DxD(2)/BxB(2)*(BxB(3)/DxD(3)))/log(alfamedio(2));

% Utilizziamo le correlazioni di Underwood per determinare i valori assunti
% da teta e dal rapporto di riflusso minimo (Rmin):

teta0=1.5;
teta=fzero('fdEs4aIC',teta0);
Rmin=(sum((alfamedio.*xD)./(alfamedio-teta)))-1;
R=1.5*Rmin;

% Ora che � stato determinato il rapporto di riflusso minimo, � possibile
% determinare il valore assunto dal numero di stadi della colonna di
% distillazione mediante le correlazioni di Underwood e Gilliland:

FR=(R-Rmin)/(R+1);
fiEd=0.75-0.75*FR^0.5668;
fiMo=1-exp((1+54.4*FR)*(FR-1)/((11+117.2*FR)*sqrt(FR)));
N(1)=ceil((fiEd+Nmin)/(1-fiEd));
N(2)=ceil((fiMo+Nmin)/(1-fiMo));
disp(['Il numero di stadi teorici secondo la correlazione di Eduljee � pari a ',num2str(N(1))])
disp(['Il numero di stadi teorici secondo la correlazione di Molokanov � pari a ',num2str(N(2))])

% Determiniamo dunque la collocazione dell'alimentazione, prima con la
% correlazione di Fenske e poi con la formula di Kirkbridge

disp('------ Collocazione del piatto di alimentazione ------')

% Fenske:
Slkinf=z(2)*xB(3)/(z(3)*xB(2));
alfainf=(alfafeed.*alfafondo).^(1/2);
Nmininf=log(Slkinf)/log(alfainf(2));
NinfF=ceil(N(1)/Nmin*Nmininf);
NsF=N-NinfF;
disp(['Il valore assunto da Ninf secondo Fenske � pari a ',num2str(NinfF)])
disp(['Il valore assunto da Ns secondo Fenske � pari a ',num2str(NsF(1))])

% Kirkbridge:
NsNinf=(z(3)/z(2)*(xB(2)/xD(3))^2*(B/D))^0.206;
Ns=NsNinf*N(1)/(1+NsNinf);
NsK=ceil(Ns-0.5*log10(N(1)));
NinfK=N(1)-NsK;
disp(['Il valore assunto da Ninf secondo Kirkbridge � pari a ',num2str(NinfK)])
disp(['Il valore assunto da Ns secondo Kirkbridge � pari a ',num2str(NsK)])

% Determiniamo il carico termico al ribollitore: 
disp('------ Carico termico al ribollitore ------')

% Consideriamo un bilancio energetico sul condensatore: Vhv = (L+D)hl+Qc ed
% un bilancio energetico sull'intera colonna:
% Fhf+Qr=Bhb+Dhd+Qc. Scelgo come temperatura di riferimento quella
% dell'alimentazione, e dunque Fhf � nullo! Come prima cosa, calcoliamo le
% entalpie specifiche presente all'interno dei due bilanci energetici:
Trif=Tfeed;
L=R*D;
V=L+D;

% Come prima cosa, calcoliamo la temperatura di bolla di testa colonna.
Tbollatesta=fzero('feEs4aIC',T0);

% hl=hd: per il calcolo di tali grandezze, utilizziamo la seguente
% correlazione:
hl=cpl.*(Tbollatesta-Trif);
hd=hl; % Questo � valido in quanto il liquido uscente dal condensatore di testa colonna ha la stessa entalpia molare del distillato

% hv: per quanto riguarda il calcolo dell'entalpia molare del vapore,
% consideriamo il seguente ciclo termodinamico:

hv=cpl.*(Tnb-Trif)+DHevTbn+cpv.*(Ttesta-Tnb);

% hb:

hb=cpl.*(Tbottom-Trif);

% Otteniamo dunque che:
hvtot=sum(hv.*xD);
hltot=sum(hl.*xD);
hdtot=sum(hd.*xD);
hbtot=sum(hb.*xB);
Qc=V*(hvtot-hltot);
Qr=B*hbtot+D*hdtot+Qc;
disp(['Il valore assunto dal carico termico al ribollitore � pari a ',num2str(Qr),' [cal/s]'])



