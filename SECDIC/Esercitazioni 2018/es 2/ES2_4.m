%COMPITO
%SensitivitÓ di reazione

close all
clear all
clc

global psi
                                          
for psi = 0.35:0.05:0.65                              %ciclo per scorrere i diversi valori di psi
    [t,output] = ode45(@runaway,[0 10], [0 1]);       %integro
    z = output(:,1);
    teta = output(:,2);
    
    hold on
    figure(1)                                         %grafico z
    plot(t,z)
    title('Grafico z')
    
    figure(2)                                         %grafico teta
    plot(t,teta)
    title('Grafico teta')
end

figure(1)                                             %aggiungo le legende alle due figure, scorrendo
legend('0.35','0.4','0.45','0.5','0.55','0.6','0.65') % i diversi valori di psi a mano <-- N.B.
figure(2)
legend('0.35','0.4','0.45','0.5','0.55','0.6','0.65')


function yp = runaway(~,y)                            %funzione ode
global psi

n = 1;
B = 20;
eps = 0.05;
h = exp(y(2)/(1+eps*y(2)));

yp(1,1) = psi/B*((1-y(1))^n)*h;
yp(2,1) = psi*((1-y(1))^n)*h-y(2);
end