%% Risoluzione dell'esercitazione n.(4) di Impianti Chimici: Distillazione multicomponente (Mattia Usuelli)

global q z P F AntA AntB AntC xD xB alfamedio teta1 teta2 Rmin xD2mRmin xD2mNmin DxD xD2m1


%% Secondo caso: componenti distribuiti
% Dati del problema: si riferiscono al vettore S=[n-pentano n-esano 2-metilesano n-eptano n-ottano];
q=1; % la miscela � liquida al punto di bolla, e di conseguenza il fattore entalpico � unitario
z=[0.04 0.40 0.02 0.48 0.06];
RRV=[1 0.98 0.5 0.01 0]; % Assumiamo che tutto l'n-pentano vada in testa, e che tutto l'n-ottano vada in coda, 
% ed ipotizziamo che l'RRV del 2-metilesano sia pari a 0.5 
RRL=1-RRV;
P=1; % [atm]
F=100; % [mol/s]: � una mia assunzione
AntA=[15.8333 15.8366 15.8261 15.8737 15.9426];
AntB=[2477.07 2697.55 2845.06 2911.32 3120.29];
AntC=[-39.94 -48.78 -53.60 -56.51 -63.63];
Tnb=[309.2 341.9 363.2 371.6 398.8]; % [K]

% Risoluzione dell'esercitazione:
DxD=RRV.*F.*z;
BxB=RRL.*F.*z;
D=sum(DxD);
B=sum(BxB);
xD=DxD./D;
xB=BxB./B;

% Determinazione della temperatura di bolla dell'alimentazione:
T0=300;
Tfeed=fzero('faEs4aIC',T0);

% Determinazione della temperatura di bolla del fondo colonna:
Tbottom=fzero('fbEs4aIC',T0);

% Determinazione della temperatura di rugiada di testa colonna:
Ttesta=fzero('fcEs4aIC',T0);

% Come prima cosa, calcoliamo la temperatura di bolla di testa colonna:
Tbollatesta=fzero('feEs4aIC',T0);

% Determinazione della volatilit� relativa dei diversi composti rispeto al
% n-eptano:
% Alimentazione:
alfafeed=(exp(AntA-AntB./(Tfeed+AntC)))./(exp(AntA(4)-AntB(4)/(Tfeed+AntC(4))));
% Testa:
alfatesta=(exp(AntA-AntB./(Ttesta+AntC)))./(exp(AntA(4)-AntB(4)/(Ttesta+AntC(4))));
% Fondo:
alfafondo=(exp(AntA-AntB./(Tbottom+AntC)))./(exp(AntA(4)-AntB(4)/(Tbottom+AntC(4))));

% Determino il valore assunto dalla frazione molare nel distillato di
% 2-metilesano nel caso di rapporto di riflusso totale (numero di stadi
% minimo):
alfamedio=(alfafeed.*alfatesta.*alfafondo).^(1/3);
Nmin=log(DxD(2)/BxB(2)*(BxB(4)/DxD(4)))/log(alfamedio(2));
SFhk=DxD(4)/BxB(4);
SF2m=SFhk*(alfamedio(3)^Nmin);
DxD2mNmin=F*z(3)*SF2m/(SF2m+1);
BxB2mNmin=F*z(3)/(SF2m+1);
Dmin=DxD(1)+DxD(2)+DxD(4)+DxD(5)+DxD2mNmin;
xD2mNmin=DxD2mNmin/Dmin;

% Determino ora il valore assunto dalla frazione molare nel distillato di
% 2-metilesano nel caso di rapporto di riflusso minimo (numero di stadi
% tendente ad infinito):
teta01=1.5;
teta1=fzero('fdEs4aIC',teta01);
teta02=1.2;
teta2=fzero('fdEs4aIC',teta02);
X0=[0.5 1];
X=fsolve('faEs4bIC',X0);
xD2mRmin=X(1);
Rmin=X(2);
R=1.5*Rmin;

% Determino ora i coefficienti di a e di b, utili poi a determinare la
% frazione molare di xD2m nel distillato:
X=fsolve('fbEs4bIC',X0);
a=X(1);
b=X(2);
xD2m1=a+b*(R/(R+1));

% Calcoliamo dunque i nuovi valori assunti da D e da RRV2m:
D0=50;
D1=fzero('fcEs4bIC',D0);
xD1(1)=DxD(1)/D1;
xD1(2)=DxD(2)/D1;
xD1(3)=xD2m1;
xD1(4)=DxD(4)/D1;
xD1(5)=DxD(5)/D1;
RRV1=D1.*xD1./(F.*z);