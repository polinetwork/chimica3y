function F=f3b2Es2bIC(X)

global z k2

F=sum(z.*(k2-1)./(1+X.*(k2-1)));