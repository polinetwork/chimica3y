function F=f2Es2bIC(X)

global z A B C P alfa

k=(exp(A-B./(X+C)))/760/P;

F=sum(z.*(k-1)./(1+alfa.*(k-1)));