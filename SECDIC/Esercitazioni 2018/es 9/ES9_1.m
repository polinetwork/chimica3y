%Simulazione open loop

close all
clear all
clc

%Dati
Fi = 9.4;
A1 = 30;
r = 1.2;
A2 = 50;
h0 = 3;

%Integrazione
hand = @(t,h)tanks(t,h,A1,A2,Fi,r);
opt = odeset('RelTol',1e-8,'AbsTol',1e-6);
[t,h] = ode45(hand,[0 60*8],[h0 h0],opt);

hold on
plot(t,h(:,1))
plot(t,h(:,2))
legend('serb. 1','serb. 2')

function hp = tanks(t,h,A1,A2,Fi,r)

if t > 5*60
    Fi = 2*Fi;
end

F1 = (h(1)-h(2))/r;
Fo = 1.43*h(2);

hp(1,1) = (Fi-F1)/A1;
hp(2,1) = (F1-Fo)/A2;

end