function dhdt=f2Ex1Es3(t,h)
global Fin Fout Abase H

if h<H
dhdt=(Fin-Fout)/Abase;
else
    dhdt=0;
end