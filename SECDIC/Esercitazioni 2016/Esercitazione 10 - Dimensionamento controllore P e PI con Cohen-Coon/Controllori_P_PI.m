clear all
clc
close all

global Fino A1 A2 R1 Kc tauI h2sp span

Fino=9.4; %m3/s
A1=30;%m2
A2=50;
R1=1.2;%s/m2
h2sp=6.6;
span=500; 

options=odeset('AbsTol',1e-10,'RelTol',1e-8);
%%anello aperto (non controllato)
[t,y]=ode113('ODEaperto',[0 4*span],[3 3],options);
h1open=y(:,1);
h2open=y(:,2);
plot(t/60,h1open,'-',t/60,h2open,'r-')
grid on
xlabel('t [min]')
ylabel('h1 (blue), h2 (red), [m]')
title('Open Loop')

%parametri CohenCoon trovati: ?? B=13.15, S=3.375
K=13.15/2; 
td=10;% s 
tau=3.8963 ;

KcP=1/K*tau/td*(1+td/(3*tau))
display(KcP);

KcPI=1/K*tau/td*(0.9+td/(12*tau))
tauIPI=td*(30+3*td/tau)/(9+20*td/tau)

%%anello chiuso (P) /punto2
Kc=KcP; 
[t,y]=ode113('ODEchiusoP',[0 4*span],[3 3],options);
h1chiusoP=y(:,1);
h2chiusoP=y(:,2);
figure(2)
subplot(1,2,1)
plot(t/60,h2chiusoP,'r-')
grid on
xlabel('t [min]')
ylabel('h2 [m]')
title(['Closed Loop, Kc=',num2str(Kc)])

%anello chiuso (PI) /punto3
Kc=KcPI;
tauI=tauIPI; 
[t,y]=ode113('ODEchiusoPI',[0 4*span],[3 3 0],options);
h1chiusoPI=y(:,1);
h2chiusoPI=y(:,2);
subplot(1,2,2)
plot(t/60,h2chiusoPI,'r-')
grid on
xlabel('t [min]')
ylabel('h2 [m]')
title(['Closed Loop, Kc=',num2str(Kc),', tauI=',num2str(tauI)])