function [dydt]=fEx2aEs11(t,y)

global FvSS Kc taui Tin tau DHevm ro V cp tcambioSP Tout2 

T=y(1);
integrale=y(2);

if t<tcambioSP
    Fv=FvSS; % condizioni di stato stazionario
    epsi=0;
else
    epsi=Tout2-T;
    Fv=FvSS+Kc*epsi+Kc/taui*integrale; % espressione della variabile manipolata
    FvMin=0; FvMax=2.*FvSS;
    Fv=max(Fv,FvMin); Fv=min(Fv,FvMax);
end

dydt(1)=(Tin-T)/tau+DHevm*Fv/(ro*V*cp);
dydt(2)=epsi; % calcolo automatico dell'integrale epsi(t)*dt
dydt=dydt'; % Comando che serve a trasformare il vettore riga in un vettore colonna
end