clc
clear all
close all

global k Keq beta gamma radice tm epsilon

k=7.93e-6; % [m^3 s / kmol]
Keq=2.93;
A0=10.198; % [kmol/m^3]
B0=3.91; % [kmol/m^3]
Q0=17.567; % [kmol/m^3]
alfa=Keq*A0*B0; 
beta=(A0+B0)*Keq+Q0;
gamma=1-Keq;
radice=sqrt(beta^2+4*alfa*gamma);
tm=3600; % [s]
epsilon=(beta+radice)/(beta-radice);
V=59.3; % [m^3]

teta0=5;

teta=fsolve('fEs11PdIC',teta0);

disp(['Il valore assunto da teta � pari a ',num2str(teta)])

prod=(V/(tm*(teta+1)))*(-(beta+radice)+(beta-radice)*epsilon*exp((radice*k*tm/Keq)*teta))/(2*gamma-2*gamma*epsilon*exp((radice*k*tm/Keq)*teta));

disp(['Il valore assunto dalla produttivit� in [kmol/s] � pari a ',num2str(prod)])
disp(['Il valore assunto dalla produttivit� in [tonnellate/giorno] � pari a ',num2str(prod*88*(24*3600)/1000)])
