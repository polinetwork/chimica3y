function dhdt=fEx3Es6(t,h)

global Fin A epsilon hlin i

Fout=epsilon*(1/2)*(hlin(i)^(-(1/2)))*(h-hlin(i));

dhdt=(Fin-Fout)/A;