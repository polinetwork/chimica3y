
% Author: Andrea Landella
%
% Calcolo dell'equilibrio liquido-vapore con metodo phi-phi

clc
close all
clearvars

global NS NVLE SVLE NINC SINC Zck Kik

% Variabili fisiche:
R = 8.3144621  ;%[Pa*m^3/(mol*K)]
    
% load database
load('COMP_v1.mat');



%% USER INPUT

% componenti scelti, mettendo sempre la virgola
COMP_NAME = ["n-esano", "n-eptano", "idrogeno", "i-pentano"];

T = 300     ;%[K]
P = 1e5     ;%[Pa]

% initial molar vector 
F0 = [4; 6; 2; 5]  ;%[mol/s]




%% CHECKS

% initial molar fraction vector, with consistency check
x0 = F0/sum(F0);
%
if sum(x0) < 1-1e-10 && sum(x0) > 1+1e-10 
    fprintf("Error! Initial molar fraction vector not unitary\n");
    return
end

%% Assign variables from database

% numero di componenti scelti
NS  = numel(COMP_NAME);

% allocazione variabili
Tck = zeros(NS,1);
Pck = zeros(NS,1);
omk = zeros(NS,1);

% vettore nomi dei componenti nel database
COMP_LIST_NAME = string(COMP_LIST(1,2:end));

% numero di componenti nel database
COMP_LIST_NUM = numel(COMP_LIST_NAME);

% ricerca entries ed assegnazione variabili
for i=1:NS
    
    COMP_FLAG = 0;
    
    % per ogni componente scelto, verifica se esiste nel database
    for j=1:COMP_LIST_NUM
        
        if strcmp(COMP_LIST_NAME(j),COMP_NAME(i)) == 1
            break
        else
            COMP_FLAG = COMP_FLAG + 1;
        end
        
    end
    
    if COMP_FLAG == COMP_LIST_NUM
        fprintf('Error! Component "%s" not found in database\n',COMP_NAME(i));
        return
    end
    
    % assegnazione variabili
    Tck(i) = COMP_LIST{2,j+1};
    Pck(i) = COMP_LIST{3,j+1};
    omk(i) = COMP_LIST{4,j+1};
    
end

% print input
fprintf("Mixture Properties:\n\n");
fprintf("T:\t%.2f\t[K]\nP:\t%.2f\t[bar]\n",T,P/1e5);
fprintf("\n");
%
fprintf("Mixture Component List:\n\n");
fprintf("\t%s\n", COMP_NAME);
fprintf("\n");

%% cancellare variabili di ricerca nel database

clear COMP_FLAG COMP_LIST COMP_LIST_NAME COMP_LIST_NUM
        
%% check iniziali sulla criticit�

% Assumption: Se un componente � supercritico, � incondensabile
SINC = []; % incondensabili
SVLE = []; % condensabili
SLIQ = []; % inevaporabili

ki = 0;
kv = 0;

for j=1:NS
    
    if T >= Tck(j)
        fprintf("Warning! Component %d is supercritical, T = %.2f > Tc = %.2f\n",j,T,Tck(j));
        ki = ki + 1;
        SINC(ki) = j; %#ok<SAGROW>
        
    elseif P >= Pck(j)
        fprintf("Warning! Component %d is supercritical, P = %.2f > Pc = %.2f\n",j,P,Pck(j));
        ki = ki + 1;
        SINC(ki) = j; %#ok<SAGROW>
        
    else
        kv = kv + 1;
        SVLE(kv) = j; %#ok<SAGROW>
    end
end

NINC = ki;
NVLE = kv;

%
Tcmix = x0'*Tck; % T pseudocritica miscela
Pcmix = x0'*Pck; % P pseudocritica miscela

if T >= Tcmix
    fprintf("Warning! Mixture is supercritical, T = %.2f > Tc = %.2f\n",T,Tcmix);
    return
elseif P >= Pcmix
    fprintf("Warning! Mixture is supercritical, P = %.2f > Pc = %.2f\n",P,Pcmix);
    return
end

%% critical properties and interaction parameters

% Calcolo della Z critica, per l'EOS PR � un valore costante, analitico 
% pari a 0.321379, usare come check di correttezza del calcolo delle phi
Zck = zeros(NS,1);
for j=1:NS
    [~, ~, Zck(j), ~] = phi_PR(Tck(j),Pck(j),Tck,Pck,omk,j,'V');
end

% calcolo del volume critico per specie
vck = zeros(NS,1)   ;%[m3/mol]
for j=1:NS
    vck(j) = Zck(j)*R*Tck(j)/Pck(j);
end

% parametri di interazione binaria, stima da Chueh & Prausnitz (1967), si
% ha una matrice simmetrica, metodo di calcolo ottimizzato
Kik = zeros(NS);
for j=1:NS
    for k=j:NS
        Kik(j,k) = 1 - ( 2*sqrt( vck(j)^(1/3)*vck(k)^(1/3) )/( vck(j)^(1/3) + vck(k)^(1/3) ) )^3;
        Kik(k,j) = Kik(j,k);
    end
end

%% calculation

return

[xL, xV, alf] = calc_RR_VLE(T,P,x0,Tck,Pck,omk)     %#ok<NOPTS>








%% Function Declaration

% parametri dell'RKS per componente puro
%     % Variabili fisiche:
%     Som  = 0.47979 + 1.576*omk - 0.1925*omk^2 + 0.025*omk^3;    
%     ktom = ( 1 + Som*(1 - sqrt(T/Tck)) )^2;
%     %
%     a = 0.42748*(R*Tck)^2/Pck*ktom;
%     b = 0.08664*(R*Tck)/Pck;
%     %
%     A = a*P/(R*T)^2;
%     B = b*P/(R*T);
%     %
%     Vec = [1 -1 A-B-B^2 -A*B];
%     VecRis = roots(Vec);
%     VecRt  = VecRis(imag(VecRis) == 0);
%     %
%     zliq = min(VecRt);
%     zvap = max(VecRt);
%     %
%     if strcmp(fase,'L') == 1
%         Z = zliq;
%     elseif strcmp(fase,'V') == 1
%         Z = zvap;
%     end
%     
%     % Energia Libera Residua per RT:
%     gRRT = Z - 1 - A/B*log(1 + B/Z) - log(Z - B);
%     
%     % Coefficiente di fugacit�:
%     f = exp(gRRT);

% calcolo phi componente puro PR
function [A, B, Z, phi] = phi_PR(T,P,Tck,Pck,omk,tipo,fase)% T in [K], P in [Pa]

    % Variabili fisiche:
    R = 8.3144621  ;%[Pa*m^3/(mol*K)]

    Tc = Tck(tipo); Pc = Pck(tipo); om = omk(tipo);
               
    Som  = 0.37464+1.54226*om-0.26992*om^2;
    ktom = (1+Som*(1-sqrt(T/Tc)))^2;
    a = 0.45724*(R*Tc)^2*ktom/Pc;
    b = 0.07780*(R*Tc)/Pc;
    A = a*P./(R*T).^2;
    B = b*P./(R*T);
    Vec = [1 -1+B A-2*B-3*B.^2 -A*B+B.^2+B.^3];
      
    Zvec = solve_cubic(Vec);
        
    if strcmp(fase,'L') == 1
        Z = Zvec(1);
    elseif strcmp(fase,'V') == 1
        Z = Zvec(3);
    end
    
    % Energia Libera Residua:
    gR = R*T*(Z-1-A/(2*sqrt(2)*B)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))))-log(Z-B));
    
    % Coefficiente di fugacit�:
    phi = exp(gR/(R*T));
end

% calcolo phi miscela nonideale PR
function [Zmix, phi] = phi_PR_mix(T,P,xsys,Tck,Pck,omk,tipo,fase)% T in [K], P in [Pa]
    %       
    global NS Kik
    
    [A, B, ~, ~] = phi_PR(T,P,Tck,Pck,omk,tipo,fase);
        
    % Preallocazione di ogni variabile:
    Am = zeros(NS); Bm = zeros(NS);
    
    % Utilizzo delle regole di combinazione:
    for i=1:NS
        [Ai, Bi, ~, ~] = phi_PR(T,P,Tck,Pck,omk,i,fase);
        for j=1:NS
            [Aj, Bj, ~, ~] = phi_PR(T,P,Tck,Pck,omk,j,fase);
            %
            Am(i,j) = sqrt(Aj*Ai)*(1 - Kik(i,j));
            Bm(i,j) = Bi/2+Bj/2;
        end
    end
    
    % Utilizzo delle regole di miscelazione:
    Amix = xsys'*Am*xsys;
    Bmix = xsys'*Bm*xsys;
    
    % Calcolo del coefficiente di compressibilit� della miscela:
    Vec = [1 -1+Bmix Amix-2*Bmix-3*Bmix.^2 -Amix*Bmix+Bmix.^2+Bmix.^3];
    
    Z = solve_cubic(Vec);
        
    if strcmp(fase,'L') == 1
        Zmix = Z(1);
    elseif strcmp(fase,'V') == 1
        Zmix = Z(3);
    end
    
    lnphi = B*(Zmix-1)/Bmix+Amix/(2*sqrt(2)*Bmix)*(B/Bmix-2*sqrt(A/Amix))...
        *log((Zmix+Bmix*(1+sqrt(2)))/(Zmix+Bmix*(1-sqrt(2))))-log(Zmix-Bmix);
    phi = exp(lnphi);
end

%% Solver function

% calcolo dell'espressione di Rachford-Rice
function [xL, xV, f] = calc_RR_VLE(T,P,zin,Tck,Pck,omk)% T in [K], P in [Pa]

    global NS NVLE SVLE NINC SINC Zck 
    
    % standard, solo sistema VLE
    tipo = 0;
    
    % se presenti supercritici incondensabili
    if NINC > 0
        tipo = 1;
    elseif NINC == 0
        tipo = 2;
    elseif NINC > 0
        tipo = 3;
    end
    
    % calcolo di un'alfa di primo tentativo, con miscela ideale
    Kj = zeros(NS,1);
    
    NS_liq_inv = 0;
    NS_vap_inc = 0;
    
    for j=1:NS
        [~, ~, ZL, phi_L] = phi_PR(T,P,Tck,Pck,omk,j,'L');
        [~, ~, ZV, phi_V] = phi_PR(T,P,Tck,Pck,omk,j,'V');               
        %
        if abs(ZL-ZV) < 1e-5
            % composto singolo � incondensabile o inevaporabile
            if ZL < Zck(j)
                % mi trovo in liq inevaporabile
                Kj(j) = 0;
                NS_liq_inv = NS_liq_inv + 1;
            else
                % mi trovo in vap incondensabile
                Kj(j) = 1e10;
                NS_vap_inc = NS_vap_inc + 1;
            end
        else
            % mi trovo in condizione di VLE
            Kj(j) = phi_L / phi_V;
        end
    end    
             
    Kj
    NS_liq_inv
    NS_vap_inc
    
    return
    
    
    if NS_liq_inv == NS
        fprintf("Mixture is Subcooled Liquid\n");
        xL = zin;
        xV = zeros(NS,1);
        f = 0;
        return
    end
    
    if NS_vap_inc == NS
        fprintf("Mixture is Superheated Vapor\n");
        xL = zeros(NS,1);
        xV = zin;
        f = 1;
        return
    end
               
    % soluzione RR           
    alf_0 = solve_RR(zin,Kj,0.5,tipo);
            
    % ciclo while per alfa
    alf_del = 10;
    alf_tol = 1e-8;

    while alf_del > alf_tol

        if alf_0 <= 0
            % mi trovo in liq inevaporabile
            xL = zin;
            xV = zeros(NS,1);
            %
            for j=1:NS
                Kj(j) = 0; 
            end
        elseif alf_0 >= 1
            % mi trovo in vap incondensabile
            xL = zeros(NS,1);
            xV = zin;
            %
            for j=1:NS
                Kj(j) = 1e10; 
            end
        else   
            xL = zin./(1+alf_0*(Kj-1))   ;%[-]
            xV = Kj.*xL                  ;%[-]
            %
            for j=1:NS
                [ZL, phi_L] = phi_PR_mix(T,P,xL,Tck,Pck,omk,j,'L');
                [ZV, phi_V] = phi_PR_mix(T,P,xV,Tck,Pck,omk,j,'V');
                %
                if abs(ZL-ZV) < 1e-5
                    if ZL < Zck(j)
                        % mi trovo in liq inevaporabile
                        Kj(j) = 0;
                    else
                        % mi trovo in vap incondensabile
                        Kj(j) = 1e10;
                    end
                else 
                    % mi trovo in condizione di VLE
                    Kj(j) = phi_L / phi_V;
                end
            end  
        end
    
        % Soluzione RR
        alf_1 = solve_RR(zin,Kj,0.5,tipo);

        % differenza
        alf_del = abs(alf_1 - alf_0);

        % aggiorno il valore alfa
        alf_0 = alf_1;

    end

    if alf_1 <= 0
        %
        fprintf("Mixture is Subcooled Liquid\n");
        xL = zin;
        xV = zeros(NS,1);
        f = 0;

    elseif alf_1 >= 1
        %
        fprintf("Mixture is Superheated Vapor\n");
        xL = zeros(NS,1);
        xV = zin;
        f = 1;

    else
        fprintf("Mixture is VLE system\n");
        f = alf_1;
    end
end

%% Solver Functions Declaration

% funzione di calcolo dell'alfa dalla rachford-rice in modo semi-analitico
function f = solve_RR(zin,Kj,alf_0,tipo)

    switch tipo
        case 0 % tipo = 0, miscela solo con VLE
            F_alf_fun  = @(alf) sum( zin.*(Kj-1)./(1 + alf*(Kj-1)) );      
            dF_alf_fun = @(alf) sum( zin.*((Kj-1).^2)./(1 + alf*(Kj-1)).^2 );  
            
        case 1 % tipo = 1, miscela con VLE e NINC
            F_alf_fun  = @(alf) sum( zin.*(Kj-1)./(1 + alf*(Kj-1)) );      
            dF_alf_fun = @(alf) sum( zin.*((Kj-1).^2)./(1 + alf*(Kj-1)).^2 );  
            
        case 2 % tipo = 2, miscela con VLE e NLIQ
            F_alf_fun  = @(alf) sum( zin.*(Kj-1)./(1 + alf*(Kj-1)) );      
            dF_alf_fun = @(alf) sum( zin.*((Kj-1).^2)./(1 + alf*(Kj-1)).^2 );  
            
        case 3 % tipo = 3, miscela con VLE e NINC e NLIQ
            F_alf_fun  = @(alf) sum( zin.*(Kj-1)./(1 + alf*(Kj-1)) );      
            dF_alf_fun = @(alf) sum( zin.*((Kj-1).^2)./(1 + alf*(Kj-1)).^2 );  
            
        case 4 % tipo = 4, miscela solo con NINC e NLIQ
            f  = sum( zin.*(Kj-1)./(1 + alf*(Kj-1)) );      
            return
            
        case 5 % tipo = 5, miscela solo con NINC
            f = 1;
            return
            
        case 6 % tipo = 6, miscela solo con NLIQ
            f = 0;
            return
    end   
    
    % procedura di newton
    
    Nit = 0;
        
    % dummy initial variables
    del_alf = 10;
    %
    F_alf = 10;
    
    % tolerances
    alf_tol = 1e-8;
    %
    F_tol = 1e-8;
    %
    Nit_max = 10000;
    
    % ciclo iterativo
    while (del_alf > alf_tol && abs(F_alf) > F_tol) && Nit < Nit_max
        
        Nit = Nit + 1;
        
        % funzione e jacobiano
        F_alf  = F_alf_fun(alf_0);      
        dF_alf = dF_alf_fun(alf_0);
        
        % update newton
        alf_1 = alf_0 + F_alf/dF_alf; 
        
        % delta
        del_alf = abs(alf_1 - alf_0);
        
        % update alfa
        alf_0 = alf_1;
        
    end

    f = alf_1;

end

% funzione di calcolo accelerata ed accurata della cubica, custom
function f = solve_cubic(vec)
    
    f   = [0;0;0];
    g_0 = f; 
    g_1 = f; 
    g_2 = f;
    
    d1 = vec(2);
    d2 = vec(3);
    d3 = vec(4);
    
    Q = (d1^2 - 3*d2)/9;
    
    R = (2*d1^3 - 9*d1*d2 + 27*d3)/54;
    
    if R^2 <= Q^3
        % has 3 real roots
        teta = acos(R/sqrt(Q^3));
        
        g_0(1) = - 2*sqrt(Q)*cos(teta/3         ) - d1/3;
        g_0(2) = - 2*sqrt(Q)*cos(teta/3 + 2*pi/3) - d1/3;
        g_0(3) = - 2*sqrt(Q)*cos(teta/3 - 2*pi/3) - d1/3;
        
        % metodo di affinazione (Ind. Eng. Chem. Res. 2012, 51, 6972?6976)        
        N_max = 1000;
        N_it = 0;
        
        % metodo di Steffensen (ref 15 articolo sopra)        
        for N_it=1:N_max 
            
            % g1 calc
            %alf = g_0(1);
            bet = g_0(2);
            gam = g_0(3);
            %
            g_1(1) = -d1-(bet+gam);
            g_1(2) = (d2-g_1(1)*gam)/(g_1(1)+gam);
            g_1(3) = -d3/(g_1(1)*g_1(2));
    
            % g2 calc
            %alf = g_1(1);
            bet = g_1(2);
            gam = g_1(3);
            %
            g_2(1) = -d1-(bet+gam);
            g_2(2) = (d2-g_2(1)*gam)/(g_2(1)+gam);
            g_2(3) = -d3/(g_2(1)*g_2(2));
            
            % g3 calc and update
            den = g_2 - 2*g_1 + g_0;
            %
            for j=1:3
                if den(j) == 0
                    f = g_2;
                    break
                end
                g_3 = g_0 - (g_1 - g_0).^2./den;
            end
            if f == g_2
                break
            end
            
            if norm(g_3 - g_0) < 1e-10
                f = g_3;
                break
            end
                       
            g_0 = g_3;
        end
        %
        if N_it == N_max
            fprintf("Error! Iter exceeded. Steffensen Method failed.\n");
            f = -99999*ones(3,1);
        end        
    else
        % may have imaginary roots
        A = - sign(R)*(abs(R) + sqrt(R^2 - Q^3))^(1/3);      
        if A == 0
%            B = 0;
            f(1) = - d1/3;
            f(2) = f(1);
            f(3) = f(1);
        else
            B = Q/A;           
            if A == B
                % 2 soluzioni reali, punto particolare
                f(1) =       (A+B) - d1/3;
                f(2) = - 1/2*(A+B) - d1/3;
                f(3) = f(2);
            else
                % 1 soluzione reale
                f(1) =       (A+B) - d1/3;
                f(2) = f(1);
                f(3) = f(1);                
            end
        end
%         f(1) =       (A+B) - d1/3;
%         f(2) = - 1/2*(A+B) - d1/3 + 1i*sqrt(3)/2*(A-B);
%         f(3) = - 1/2*(A+B) - d1/3 - 1i*sqrt(3)/2*(A-B);
    end
    
    % Sorting values, in ASCENDING order (low->high)
    if f(1) < f(2)
        if f(2) > f(3)
            if f(1) < f(3)
                temp = f(2);
                f(2) = f(3);
                f(3) = temp;
            else
                temp = f(1);
                f(1) = f(3);
                f(3) = f(2);
                f(2) = temp;
            end
        end
    else
        if f(2) < f(3)
            if f(1) < f(3)
                temp = f(1);
                f(1) = f(2);
                f(2) = temp;
            else
                temp = f(1);
                f(1) = f(2);
                f(2) = f(3);
                f(3) = temp;
            end
        else
            temp = f(1);
            f(1) = f(3);
            f(3) = temp;
        end
    end    
end


