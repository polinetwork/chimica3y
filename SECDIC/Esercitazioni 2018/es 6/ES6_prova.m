close all  %N.B. chiude anche le figure
clear all
clc

%Dati
A = 3;
g = 9.81;
xi = 0.8*sqrt(2*g);
L0 = 7;

%Eulero
t = 0:0.1:5;
h = zeros(1,length(t));
h(1) = L0;
for i = 2:length(h)
    h(i) = h(i-1) + 0.1*(-xi/A*sqrt(h(i-1)));
    if h(i) <= 0
        h(i) = 0;
    end
end

%Modello linearizzato                 SBAGLIATO, vedere ES6
hl = zeros(1,length(t));
dhdt0 = -xi/A*sqrt(L0);
for j = 1:length(t)
    hl(j) = L0 + dhdt0*(t(j)-t(1));
end

%ODE
hand = @(time,H)fun(time,H,xi,A);
options = odeset('Events',@evento);  %N.B.
[time,H] = ode45(hand,[0 5],L0,options);

hold on
plot(t,h,'--')
plot(t,hl,'-.')
plot(time,H)
legend('Eulero','Lineare','ODE')

function hp = fun(~,h,xi,A)
hp = -xi/A*sqrt(h);
end

%funzione EVENTO
function [valore, terminare, direzione] = evento(t,H)

if H <= 0.001 %non 0, ma valore molto piccolo
    valore = 0; %0 � evento verificato (dalle istruzioni)
else
    valore = 1;
end

    terminare = 1; %1 cio� true, si ferma se H � negativa
    direzione = 0; %0 cio� false, non mi interessa la direzione da cui ci arrivo
end