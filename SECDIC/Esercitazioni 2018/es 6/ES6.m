close all  %N.B. chiude anche le figure
clear all
clc

%Dati
A = 3;
g = 9.81;
xi = 0.8*sqrt(2*g);
L0 = 7;

%Eulero
dt = 0.05;
t = 0:dt:5;
h = zeros(1,length(t));
h(1) = L0;
for i = 1:(length(h)-1)
    h(i+1) = h(i) + dt*(-xi/A*sqrt(h(i)));
    if h(i+1) <= 0
        h(i+1) = 0;
    end
end

%ODE
hand = @(time,H)fun(time,H,xi,A);
[time,H] = ode45(hand,[0 5],L0);

%Linearizzato
handL = @(tem,hl)lin(tem,hl,xi,A,L0);
[tem,hl] = ode45(handL,[0 5],L0);

hold on
plot(t,h,'--')
plot(time,H)
plot(tem,hl,'-.')
legend('Eulero','ODE','Linearizzato')


function hp = fun(~,h,xi,A)
hp = -xi/A*sqrt(h);
end

function hp = lin(~,h,xi,A,L0)
hp = -xi/A*(sqrt(L0)+1/(2*sqrt(L0))*(h-L0));
end