function F=fdEs14IC(fc)

global Rec4

F=1/sqrt(fc)-4*log10(Rec4*sqrt(fc))+0.4;