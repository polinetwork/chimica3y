function f=ODEP(t,h)

global A cd Fin Ad g hsp Kc span

if t<5*span
    Fout=Ad*cd*sqrt(2*g*h);
else
    Fout=Fin+Kc*(hsp-h);
end

f=(Fin-Fout)/A;