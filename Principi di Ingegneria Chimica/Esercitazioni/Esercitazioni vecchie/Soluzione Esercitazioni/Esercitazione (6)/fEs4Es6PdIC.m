function F=fEs4Es6PdIC(X)

global hi Ta Tb D

F=hi*(X-Tb)-(1.30225/D^(1/4))*(Ta-X)^(5/4);