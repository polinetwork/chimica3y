%% Risoluzione dell'esercitazione (13) di P.d.I.C.

clc
clear all
close all

% Dati:
maf=0.31710; %[Kg/s]
P=101325; %[Pa]
A1=11.5e12/3600; %[1/s]
A2=3.84e14/3600; %[1/s]
DH1=-17.91e5; %[J/mol]
DH2=-5.02296e6; %[J/mol]
x0n=6e-3;
x0O2=1e-1;
x0N2=0.894;
Tin=693.15; %[K]
v=8; %[m/s]
Te=693.15; %[K]
U=581.361; %[W/m^2 K]
cp=1004.592; %[J/kg K]
Xin=0.95;
Tmax=833.15;
R1=8.314; %[J/mol K]
R2=1.986; %[cal/mol K]
D=0.021; %[m]
PMaf=148e-3; %[kg/mol]
naf=maf/PMaf; %[mol/s]
sigmanaf=(A1*exp(-27000/(R2*763.15)))/(A1*exp(-27000/(R2*763.15))+A2*exp(-35000/(R2*763.15)));
ntot=naf/(Xin*sigmanaf*x0n); %[mol/s]
PMin=(x0n*(10*12+8*1)+x0O2*32+x0N2*28)*1e-3; %[kg/mol]
romix=P*PMin/(R1*Tin); %[kg/m^3]
Vin=ntot*PMin/romix; %[m^3/s]
Atubo=D^2*pi/4; %[m^2]
Vtubo=Atubo*v; %[m^3/s]
ntubi=ceil(Vin/Vtubo);

% Risoluzione: Determiniamo ora i profili termici e di concentrazione
% all'interno del tubo
z(1)=0;
Xi(1)=0;
T(1)=Tin;
dXidz(1)=(Atubo*ntubi/ntot)*(A1*exp(-27000/(R2*T(1)))+A2*exp(-35000/(R2*T(1))))*P*(1-Xi(1))/(R1*T(1));
dTdz(1)=U*(4/D)*(pi*D^2/4*ntubi)/(ntot*cp*PMin)*(Te-T(1))-(DH1*A1*exp(-27000/(R2*T(1)))+DH2*A2*exp(-35000/(R2*T(1))))*(pi*D^2/4*ntubi)/(ntot*cp*PMin)*P*x0n*(1-Xi(1))/(R1*T(1));
z(2)=z(1)+0.001;
Xi(2)=Xi(1)+dXidz(1)*(z(2)-z(1));
T(2)=T(1)+dTdz(1)*(z(2)-z(1));
dXidz(2)=(Atubo*ntubi/ntot)*(A1*exp(-27000/(R2*T(2)))+A2*exp(-35000/(R2*T(2))))*P*(1-Xi(2))/(R1*T(2));
dTdz(2)=U*(4/D)*(pi*D^2/4*ntubi)/(ntot*cp*PMin)*(Te-T(2))-(DH1*A1*exp(-27000/(R2*T(2)))+DH2*A2*exp(-35000/(R2*T(2))))*(pi*D^2/4*ntubi)/(ntot*cp*PMin)*P*x0n*(1-Xi(2))/(R1*T(2));
i=3;

while Xi(i-1)<0.95
z(i)=z(i-1)+0.001;
Xi(i)=Xi(i-1)+dXidz(i-1)*(z(i)-z(i-1));
T(i)=T(i-1)+dTdz(i-1)*(z(i)-z(i-1));
dXidz(i)=(Atubo*ntubi/ntot)*(A1*exp(-27000/(R2*T(i)))+A2*exp(-35000/(R2*T(i))))*P*(1-Xi(i))/(R1*T(i));
dTdz(i)=U*(4/D)*(pi*D^2/4*ntubi)/(ntot*cp*PMin)*(Te-T(i))-(DH1*A1*exp(-27000/(R2*T(i)))+DH2*A2*exp(-35000/(R2*T(i))))*(pi*D^2/4*ntubi)/(ntot*cp*PMin)*P*x0n*(1-Xi(i))/(R1*T(i));
i=i+1;
end

disp(['Il valore assunto dalla lunghezza del tubo � pari a ',num2str(z(i-1)),' [m]'])
subplot(1,2,1)
plot(z,Xi)
grid
xlabel('Lunghezza del reattore')
ylabel('Conversione')
title('Grafico z-Xi')
subplot(1,2,2)
plot(z,T)
grid
xlabel('Lunghezza del reattore')
ylabel('Temperatura')
title('Grafico z-T')


