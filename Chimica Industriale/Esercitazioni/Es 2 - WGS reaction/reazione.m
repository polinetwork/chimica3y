function f=reazione(lambda)

global F1i P K v

F2i=F1i+lambda*v;
x2=F2i/sum(F2i);

f=K-prod((P*x2).^v);