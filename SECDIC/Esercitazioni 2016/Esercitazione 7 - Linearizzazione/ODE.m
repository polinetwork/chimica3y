function f=ODE(t,h)

global A csi

if h>=0
    f=-csi*sqrt(h)/A;
else
    f=0;    
end
end