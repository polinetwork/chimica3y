clear all
clc

global P DHr cpi v ko E Fin

Tin=733; %K
%[SO2 O2 SO3 N2]
Fin=74*[0.07 0.109 0 0.821]; %mol/s
P=1; %atm
DHr=-22942*4.186; %J/mol
cpi=[12.55 6.75 18.9 7.53]*4.186; %J/molK
v=[-1 -0.5 1 0];

%dati cinetici
ko=[1.33e5 4.09e8];
E=[128e3 206e3];%J/mol

options=odeset('MaxStep',1);
[t,y]=ode23('sistema2',[0 800],[0 Tin], options);
subplot(1,2,1)
plot(t,y(:,1))
grid on
ylabel('conversione [-]');
xlabel('Wcat [kg]');
subplot(1,2,2)
plot(t,y(:,2))
grid on
ylabel('T [K]');
xlabel('Wcat [kg]');

Fin
conv=y(length(y),1)
Fout=Fin+v.*Fin(1)*conv
Tin
Tout=y(length(y),2)
