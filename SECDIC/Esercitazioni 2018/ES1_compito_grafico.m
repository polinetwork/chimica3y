close all
clear all
clc

y0 = [3 0 0]';
[t,y] = ode45(@eqdiff, [0 15], y0);

plot(t,y)

function yprimo = eqdiff(~,y)
k1 = 0.5;
k2 = 0.3;
yprimo(1,1) = -k1*y(1);
yprimo(2,1) = k1*y(1)-k2*y(2);
yprimo(3,1) = k2*y(2);
end