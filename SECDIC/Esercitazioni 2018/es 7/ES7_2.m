%Uguale alla precedente, con le if      DA FINIRE

close all
clear all
clc

global Fivett Fovett Fi Cd Dforo
%Dati
Dtank = 12;
H = 7;
Dforo = 1.3;
Cd = 0.616;
Fi = 8.5;
h0 = 4.72;

%Integrazione ODE
hand = @(t,h)tank(t,h,Dtank,Fi,Cd,Dforo);
handOut = @(t,h,flag)salvaF(t,h,flag);
options = odeset('AbsTol',1e-8,'RelTol',1e-8,'OutputFcn',handOut,'Refine',1);
[t,h] = ode45(hand,[0 70*60], h0, options);
L = zeros(length(h),1);
for i = 1:length(h)
    L(i) = h(i) + 0.5;
end

figure(1)
plot(t,L)
title('Livello serbatoio')
xlabel('t[s]')
ylabel('L[m]')
figure(2)
hold on
plot(t(2:end),Fivett)
plot(t(2:end),Fovett)
legend('Fi','Fo')
title('Andamento portate')
xlabel('t[s]')
ylabel('F[m3/s]')

function hp = tank(t,h,Dtank,Fi,Cd,Dforo)
if (t > 10*60) && (t <= 20*60)
    Fi = Fi*0.65;                          %N.B. --> va bene perch� Fi � data come input
elseif (t > 40*60) && (t <= 50*60)          %OGNI VOLTA, con global invece la cambierei e quindi nella if
    Fi = Fi*0.5;                            %dovrei usare un'altra variabile, una locale
end

Aserb = pi/4*Dtank^2;
Fo = Cd*pi/4*Dforo^2*sqrt(2*9.81*h);

hp = (Fi-Fo)/Aserb;
end

function status = salvaF(t,h,flag)

global Fivett Fovett Fi Cd Dforo 

status = 0;

%conf = char(zeros(size(flag,1),size(flag,2))); %char di confronto per flag = '' (lunghezza giusta)

if strcmp(flag,'')
    
    Filoc = Fi;
    if (t > 10*60) && (t <= 20*60)
     Filoc = Filoc*0.65;                          
    elseif (t > 40*60) && (t <= 50*60)          
     Filoc = Filoc*0.5;
    end

    Fo = Cd*pi/4*Dforo^2*sqrt(2*9.81*h);
    Fivett = [Fivett;Filoc];
    Fovett = [Fovett;Fo];

elseif strcmp(flag,'init')
    Fivett = [];
    Fovett = [];

end

end