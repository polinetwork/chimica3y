% esame 2010 esercizio 4 NONCHE' esame 2012 esercizio 4
% determina l'andamento della temperatura in un serbatoio riscaldato su cui
% � implementato un controllore P, e si voglia risolvere un problema di
% servomeccanismo

clear all
close all
clc

%dati
V   = 7;            %m^3
Fin = 140e-3;       %m^3/s
cp  = 0.7;            %kcal/kg/K 
dHev = 230;         %kcal/kg
Tin  = 50;          %�C
Tss  = 70;          %�C
TSP  = 80;          %�C
Kc   = 0.7;
rho  = 1000;        %kg/m^3 DEDUCO IL LIQUIDO SIA ACQUA

%portata di vapore a stazionario
tau = V/Fin;
mvSS  = rho*cp*V/dHev/tau*(Tss - Tin);      %kg/s

%risolvo ode
hSTH  = @(t,T)STH(t,T,Tin,tau,dHev,rho,cp,V,mvSS,Kc,TSP);
opts  = odeset('AbsTol',1e-8,'RelTol',1e-12);
[t T] = ode45(hSTH, [0 600], Tss, opts);
plot(t,T)

%OFFSET
offsetNum = TSP - T(end);

%risoluzione analitica
a = dHev / (rho*cp*V);
N = Tin/tau + a*mvSS + a*Kc*TSP;
D = 1/tau + a*Kc;
Tasy = N/D;
offsetAn = TSP - Tasy;
Kp = dHev/(Fin*cp*rho);

disp(['OFFSET STIMATO DA RISOLUZIONE NUMERICA ODE = ', num2str(offsetNum)])
disp(['OFFSET CALCOLATO IMPONENDO NUOVO STATO STAZIONARIO (da confronto con valore asintotico di T) = ', num2str(offsetAn)])

%ode
function dT = STH(t,T,Tin,tau,dHev,rho,cp,V,mvSS,Kc,TSP)

mv = mvSS + Kc*(TSP - T);
dT = (Tin - T)/tau + dHev / (rho*cp*V) * mv;

end

