%Serbatoio con miscelazione

close all
clear all
clc

global F2 c1 c2 V

F1_0 = 2;
c1 = 0.5;
F2 =10;
c2 = 6;
V = 0.5;

c3_0 = (F1_0*c1+F2*c2)/(F1_0+F2);      %integro per trovare c3
[t,c] = ode23s(@mixer, [0 800], c3_0);

Fin = zeros(length(t));                %scorro gli elementi del vettore t e calcolo F1 corrispondente
for i = 1:length(t)
    Fin(i) = portata(t(i));
end

figure(1)                              %due grafici, di due colori diversi, per le variabili indicate (c3, F1)
subplot(2,1,1)
plot(t,c,'b')
subplot(2,1,2)
plot(t,Fin,'g')
    

function cp = mixer(t,c)               %funzione ode (cio� il bilancio)
global F2 c1 c2 V

F1 = portata(t);

cp = (F1*c1+F2*c2-(F1+F2)*c)/V;
end

function F = portata(t)                %calcolo la portata F1 in funzione del tempo, con l'andamento specificato nei dati
F = min(2+0.04*t,20);
end