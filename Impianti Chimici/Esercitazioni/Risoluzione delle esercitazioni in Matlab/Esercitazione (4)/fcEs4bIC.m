function F=fcEs4bIC(D)

global DxD xD2m1

F=D-DxD(1)-DxD(2)-D*xD2m1-DxD(3)-DxD(4)-DxD(5);