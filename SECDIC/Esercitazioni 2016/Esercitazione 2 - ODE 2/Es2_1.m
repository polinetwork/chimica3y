function main
close all
clear all
clc
global k1 k2 k3

k1=0.5; k2=10e-7; k3=0.6;

options=odeset('RelTol',1e-8,'AbsTol',1e-12);
[t,c]=ode23s(@sistemaODE,[0 15],[1 1],options);
plot(t,c(:,1),'r',t,c(:,2),'b')
grid on
end

function f=sistemaODE(t,c)
global k1 k2 k3

B=c(1); S=c(2);

f=[k1*B*S/(k2+S);
    -k3*k1*B*S/(k2+S)];
end