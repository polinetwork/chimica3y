clear all
clc

global m cp Tino Fin Q

m=100; %kmol
cp=2500; %J/kmolK
Tino=300;
Fin=8;%kmol/s
Q=1e6; %W

[t,T]=ode45('ODE_es2',[0 500],Tino);
plot(t,T,'o-')
ylabel('Tout [K]');
xlabel('t [s]');
grid on