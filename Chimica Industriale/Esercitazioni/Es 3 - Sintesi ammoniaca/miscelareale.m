function f=miscelareale(lambda)

global Tc Pc om tipo T P Fin K v

F=Fin+lambda*v;
y=F/sum(F);

[Z,A,B,Ap,Bp,a,b,am,bm,E]=EoS_mix(T,P,y,Tc,Pc,om,tipo);
phi=phi_mix(Z(3),A,B,Ap,Bp,tipo);

ai=(P/101325)*phi.*y;

f=K-prod(ai.^v);