%% Risoluzione dell'esercitazione (6) di Impianti Chimici: Distillazione differenziale (Mattia Usuelli)
clc
clear all
close all
disp('------ Distillazione differenziale ------')

global z AntA AntB AntC P

% Dati: Si riferiscono al vettore delle specie S=[benzene toluene];
M0=100; % [kmol]
P=1; % [bar]
D=25; % [kmol/h]
z=[0.5 0.5];
% Si considerano inoltre le seguenti ipotesi semplificative:
% 1) Miscele ideali iin fase vapore ed in fase liquida
% 2) La volatilit� relativa � assunta costante e calcolata all T di bolla
% della carica alimentata
% 3) La portata di distillato D � costante e pari al valore sopracitato
% La forma della Antoine � del tipo log10Pev=A-(B/(T+C-273.15)), con T in
% [K] e Pev in [bar]
AntA=[3.98523 4.05043];
AntB=[1184.24 1327.62];
AntC=[217.572 217.625];
% Risoluzione dell'esercitazione:

% Punto 1): calcolo della massima frazione molare teorica di benzene nel
% distillato
disp('1) Calcolo della massima frazione molare teorica di benzene nel distillato')
T0=350;
Tbolla=fzero('faEs6aIC',T0);
alfa=(10^(AntA(1)-(AntB(1)/(Tbolla+AntC(1)-273.15))))/(10^(AntA(2)-(AntB(2)/(Tbolla+AntC(2)-273.15))));
% La massima frazione molare di benzene nel distillato corrisponde a quella
% calcolata all'istante iniziale. Dunque:
xDbmax=alfa*z(1)/(1+z(1)*(alfa-1));
disp(['La massima frazione molare di benzene nel distillato � pari a ',num2str(xDbmax)])

% Punto 2): Calcolo del tempo richiesto per ottenere nell'accumulatore del
% condensato una miscela con frazione molare media di benzene pari a 0.65.
% Possiamo risolvere questo punto mediante un ciclo while
disp('2) Calcolo di t, xB, B e Dprimo affinch� la frazione molare media di xD sia 0.65')
xBb(1)=z(1);
M(1)=M0*exp((1/(1-alfa))*(log(z(1)/xBb(1))+alfa*log((1-xBb(1))/(1-z(1)))));
xDb(1)=alfa*xBb(1)/(1+xBb(1)*(alfa-1));
Dprimo(1)=M0-M(1);
xDbmedio(1)=xDb(1);
t(1)=0;
deltaxBb=-0.000001;
i=1;
while xDbmedio(i)>0.65
    i=i+1;
    xBb(i)=xBb(i-1)+deltaxBb;
    M(i)=M0*exp((1/(1-alfa))*(log(z(1)/xBb(i))+alfa*log((1-xBb(i))/(1-z(1)))));
    xDb(i)=alfa*xBb(i)/(1+xBb(i)*(alfa-1));
    Dprimo(i)=M0-M(i);
    xDbmedio(i)=(M0*z(1)-M(i)*xBb(i))/(Dprimo(i));
    t(i)=Dprimo(i)/D;
end
subplot(1,2,1)
plot(Dprimo,xDb)
grid
xlabel('Dprimo [kmol]')
ylabel('xDb')
title('Grafico Dprimo-xDb')
subplot(1,2,2)
plot(Dprimo,xDbmedio)
grid
xlabel('Dprimo [kmol]')
ylabel('xDbmedio')
title('Grafico Dprimo-xDbmedio')
disp(['Il valore assunto dal tempo � pari a ',num2str(t(i)),' [h]'])
disp(['Il valore assunto da xB � pari a ',num2str(xBb(i))])
disp(['Il valore assunto da B � pari a ',num2str(M(i)),' [kmol]'])
disp(['Il valore assunto da Dprimo � pari a ',num2str(Dprimo(i)),' [kmol]'])
