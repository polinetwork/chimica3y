function F=feEs4aIC(X)

global AntA AntB AntC P xD

F=(sum((exp(AntA-AntB./(X+AntC))).*xD))-P*760;