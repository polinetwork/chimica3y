%% Secondo esercizio dell'esercitazione (1) di SECDIC
clc
clear all
close all
global k1 k2
% Dati:

Ca0=3; % [mol/m^3]
Cb0=0; % [mol/m^3]
Cc0=0; % [mol/m^3]
k1=0.5; % [s^-1]
k2=0.3; % [s^-1]
c0=[Ca0 Cb0 Cc0];
options= odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=[0 5];
[t,c] = ode45('fEx2Es1',tSpan,c0,options);

plot(t,c(:,1),'-r',t,c(:,2),'-m',t,c(:,3),'-b')
legend('ca','cb','cc')
xlabel('Tempo [s]')
ylabel('Concentrazioni delle specie, [mol/m^3]')
title('Andamento delle concentrazioni col tempo')