% Risoluzione dell'esercitazione (6) di P.d.I.C

clc
clear all
close all

%% Esercizio (4)

disp('---------- Esercizio (4) ----------')

global hi Ta Tb D De RcoibL

% Dati:
Tb=275.15;
Ta=293.15;
hi=987.95;
D=2.5e-2;

X0=270;

X=fzero('fEs4Es6PdIC',X0);

disp(['Il valore assunto dalla temperatura di parete � pari a ',num2str(X), ' [K]'])

pause

%% Esercizio (5)

disp('---------- Esercizio (5) ----------')

% Dati aggiuntivi rispetto all'esercizio (4)
ks=0.04302;
s=5e-3;
De=D+s;

RcoibL=(2/(ks*pi))*log(De/D);

X0=[280 28]';

X=fsolve('fEs5Es6PdIC',X0);

disp(['Il valore assunto da Tw1 � pari a ',num2str(X(1)), ' [K]'])
disp(['Il valore assunto da Tw2 � pari a ',num2str(X(2)), ' [K]'])