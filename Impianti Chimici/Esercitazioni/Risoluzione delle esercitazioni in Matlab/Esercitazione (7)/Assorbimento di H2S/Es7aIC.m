%% Risoluzione dell'esercitazione (7) di Impianti Chimici: Assorbimento di H2S (Mattia Usuelli)
clc
clear all
close all
disp('------ Assorbimento di H2S ------')

% Dati:
y0H2S=0.003;
y0N2=0.997;
P=760; % [mmHg]
yNH2S=0.0001;
T=294.15; % [K]
HenryH2S=500*760; % [mmHg]
% I seguenti parametri per l'Antoine si riferiscono all'H20, nella forma
% logPev=A-B/(T+C), con T in [K] e Pev in [mmHg]
AntA=18.3036;
AntB=3816.44;
AntC=-46.13;
Gtot0=100; % [kmol/h]

% Risoluzione dell'esercitazione:
Y0H2S=y0H2S/(1-y0H2S);
YNH2S=yNH2S/(1-yNH2S);
disp('Poich� frazioni e rapporti massivi sono circa uguali, usiamo il metodo delle cascate lineari')
disp('1) Determinazione della minima portata di acqua richiesta')
G=Gtot0*(1-y0H2S);
keq=HenryH2S/P;
X1eq=Y0H2S/keq;
XNpiu1H2S=0;
LGmin=(Y0H2S-YNH2S)/(X1eq-XNpiu1H2S);
Lmin=LGmin*G;
disp(['Il valore assunto dalla minima portata di acqua � pari a ',num2str(Lmin),' [kmol/h]'])
disp('   ')
disp('2) Determinazione delle frazioni molari delle correnti in uscita')
LGeff=1.2*LGmin;
X1=XNpiu1H2S+(1/LGeff)*(Y0H2S-YNH2S);
x1H2S=X1/(X1+1);
disp(['La composizione della corrente uscente H2S/acqua � pari a : ',num2str([x1H2S 1-x1H2S])])
disp(['La composizione della corrente uscente H2S/N2 � pari a : ',num2str([yNH2S 1-yNH2S])])
disp('   ')
disp('3) Determinazione del numero di stadi di equilibrio')
E=1/LGeff*keq;
YeqNpiu1=keq*XNpiu1H2S;
N=log(((YeqNpiu1-Y0H2S)-(YNH2S-Y0H2S))/((YeqNpiu1-Y0H2S)-E*(YNH2S-Y0H2S)))/log(E);
disp(['Il numero di stadi di equilibrio � pari a ',num2str(ceil(N))])
