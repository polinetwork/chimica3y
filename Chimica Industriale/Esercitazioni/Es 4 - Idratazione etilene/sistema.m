function f=sistema(X)

global K phi phio P Po v

y=[X(1) X(2) 1-X(1)-X(2)];
x=[X(3) 0 1-X(3)];

ai=P*y.*phi;

f(1)=K-prod(ai.^v);
f(2)=P*y(1)*phi(1)-Po(1)*phio(1)*x(1);
f(3)=P*y(3)*phi(3)-Po(3)*phio(3)*x(3);