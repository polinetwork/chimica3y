%NON E' NECESSARIO USARLO, VEDI GRAFICO E SCORRI LA LISTA
%Integrazione reattore batch

close all
clear all
clc

tmax = fminsearch(@reatt, 10);
disp(num2str(tmax));


%Funzione da ottimizzare (cambiata di segno!)
function cB = reatt(tf)
y0 = [3 0 0]';
[t,y] = ode45(@eqdiff, [0 tf], y0);
cB = - y(end,2);
end

%Funzione ode
function yprimo = eqdiff(t,y)
k1 = 0.5;
k2 = 0.3;
yprimo(1,1) = -k1*y(1);
yprimo(2,1) = k1*y(1)-k2*y(2);
yprimo(3,1) = k2*y(2);
end