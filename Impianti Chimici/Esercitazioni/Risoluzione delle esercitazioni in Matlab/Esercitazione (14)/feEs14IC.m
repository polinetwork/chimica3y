function F=feEs14IC(fc)

global Rec5

F=1/sqrt(fc)-4*log10(Rec5*sqrt(fc))+0.4;