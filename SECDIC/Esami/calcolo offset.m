
global V Fi Cpl dHev Ti Tss Tsp Kc rho Fvss
V=7;
Fi=0.1
Cpl=1
dHev=530
Ti=60+273.15
Tss=80+273.15
Tsp=90+273.15
Kc=1
rho=1000
Fvss=Fi*rho*Cpl*(Tss-Ti)/dHev
tspan=[0:0.01:200]
T0=Tss
[t,T] = ode23s(@eqdiff,tspan,T0);
for i= 1: length(t)
Tspo(i) = Tsp;
end
hold on
plot(t,T,'b','LineWidth',1.3) 
plot(t,Tspo,'k--','LineWidth',1.5)

offset=Tsp-T(end)
taup=V/Fi;
Kp=dHev/(rho*Cpl*Fi);
offest2=(Tsp-Tss)/(1+Kp*Kc)

Tns=fsolve(@azzeramento,300)
offset3=Tsp-Tns

function dT= eqdiff(t,T)
global V Fi Cpl dHev Ti Tss Tsp Kc rho Fvss
dT = Fi/V*(Ti-T)+dHev/(rho*Cpl*V)*(Fvss+Kc*(Tsp-T))
end % Chiudo la function

function y=azzeramento(Tns)
global V Fi Cpl dHev Ti Tss Tsp Kc rho Fvss
y=Fi/V*(Ti-Tns)+dHev/(rho*Cpl*V)*(Fvss+Kc*(Tsp-Tns))
end
