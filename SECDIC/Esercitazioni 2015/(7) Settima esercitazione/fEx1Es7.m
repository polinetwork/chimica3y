function dcadt=fEx1Es7(t,ca)

global ca0 tau k

dcadt=(ca0-ca)/tau-k*ca;