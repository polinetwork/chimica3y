%% Risoluzione dell'esercitazione (6) di Impianti Chimici: Rettifica discontinua (Mattia Usuelli)
clc
clear all
close all
disp('------ Rettifica discontinua ------')

global alfa R z xDm

% Dati: Si riferiscono al vettore delle specie S=[benzene toluene];
N=4; % tale grandezza � pari al numero di stadi teorici (piatti teorici pi� ribollitore)
M0=100; % [kmol]
P=1; % [bar]
z=[0.5 0.5];
R=1.65; % Lavoriamo per ora con una rapporto di riflusso costante
alfa=2.49930433925590; % E' la medesima calcolata nell'esercizio precedente (Distillazione differenziale)
% Si considerano inoltre le seguenti ipotesi semplificative:
% 1) Miscele ideali in fase vapore ed in fase liquida
% 2) La volatilit� relativa � assunta costante e calcolata all T di bolla
% della carica alimentata
% 3) Contodiffusione molecolare in colonna: � dunque applicabile il metodo
% di McCabe-Thiele
% La forma della Antoine � del tipo log10Pev=A-(B/(T+C-273.15)), con T in
% [K] e Pev in [bar]
AntA=[3.98523 4.05043];
AntB=[1184.24 1327.62];
AntC=[217.572 217.625];

% Risoluzione dell'esercitazione:

% Caso a): R=cost e xD variabile
disp('   ')
disp('------ Caso a) : R = cost e xD variabile ------')

% Punto 1): calcolo della massima frazione molare di benzene nel distillato
disp('Punto 1): calcolo della massima frazione molare di benzene nel distillato')
xD0=0.7;
xDmax=fzero('faEs6bIC',xD0);
disp(['Il valore assunto dalla massima frazione molare di benzene nel distillato � pari a ',num2str(xDmax)])

% Punto 2):
disp('   ')
disp('Punto 2): calcolo della composizione della condensa finale di distillato, in caso di Dprimo = 52.32 [kmol] a R=cost')
Dprimoobb=52.32; % [kmol], � un dato preso dalla risoluzione della rettifica discontinua
% Precediamo ora mediante un procedimento di tipo iterativo, che contiene
% anche un calcolo numerico degli integrali presenti:
xD(1)=xDmax;
x4(1)=xD(1)/(alfa-xD(1)*(alfa-1));
y3(1)=(R/(R+1))*x4(1)+xD(1)/(R+1);
x3(1)=y3(1)/(alfa-y3(1)*(alfa-1));
y2(1)=(R/(R+1))*x3(1)+xD(1)/(R+1);
x2(1)=y2(1)/(alfa-y2(1)*(alfa-1));
y1(1)=(R/(R+1))*x2(1)+xD(1)/(R+1);
xB(1)=y1(1)/(alfa-y1(1)*(alfa-1));
f(1)=1/(xD(1)-xB(1));
int(1)=0;
B(1)=M0;
Dprimo(1)=0;
xDmedio(1)=xD(1);
deltaxD=-0.00001;
i=1;
while Dprimo(i)<Dprimoobb
   i=i+1;
   xD(i)=xD(i-1)+deltaxD;
   x4(i)=xD(i)/(alfa-xD(i)*(alfa-1));
   y3(i)=(R/(R+1))*x4(i)+xD(i)/(R+1);
   x3(i)=y3(i)/(alfa-y3(i)*(alfa-1));
   y2(i)=(R/(R+1))*x3(i)+xD(i)/(R+1);
   x2(i)=y2(i)/(alfa-y2(i)*(alfa-1));
   y1(i)=(R/(R+1))*x2(i)+xD(i)/(R+1);
   xB(i)=y1(i)/(alfa-y1(i)*(alfa-1));
   f(i)=1/(xD(i)-xB(i));
   int(i)=((f(i)+f(i-1))/2)*(xB(i)-xB(i-1));
   B(i)=B(i-1)*exp(int(i));
   Dprimo(i)=M0-B(i);
   xDmedio(i)=(M0*z(1)-B(i)*xB(i))/(M0-B(i));
end
disp(['La composizione del distillato (benzene/toluene) � pari a ',num2str([xDmedio(i) 1-xDmedio(i)])])

% Dal momento che V ed R sono costanti, anche D lo �, ed � calcolabile come
% segue:
t=2; % [h]
D=Dprimo(i)/t;
V=D*(R+1);

disp(['Il valore assunto dalla portata di vapore V circolante in colonna � pari a ',num2str(V),' [kmol/h]'])

% Punto 3):
disp('   ')
disp('Punto 3): calcolo della quantit� di prodotto ottenuto in caso di xDmedio = 0.65 a R=cost')

while xDmedio(i)>0.65
   i=i+1;
   xD(i)=xD(i-1)+deltaxD;
   x4(i)=xD(i)/(alfa-xD(i)*(alfa-1));
   y3(i)=(R/(R+1))*x4(i)+xD(i)/(R+1);
   x3(i)=y3(i)/(alfa-y3(i)*(alfa-1));
   y2(i)=(R/(R+1))*x3(i)+xD(i)/(R+1);
   x2(i)=y2(i)/(alfa-y2(i)*(alfa-1));
   y1(i)=(R/(R+1))*x2(i)+xD(i)/(R+1);
   xB(i)=y1(i)/(alfa-y1(i)*(alfa-1));
   f(i)=1/(xD(i)-xB(i));
   int(i)=((f(i)+f(i-1))/2)*(xB(i)-xB(i-1));
   B(i)=B(i-1)*exp(int(i));
   Dprimo(i)=M0-B(i);
   xDmedio(i)=(M0*z(1)-B(i)*xB(i))/(M0-B(i));
end
disp(['La quantit� di prodotto ottenuto � pari a ',num2str(Dprimo(i))])
% Dal momento che V ed R sono costanti, anche D lo �, ed � calcolabile come
% segue:
t=2; % [h]
D=Dprimo(i)/t;
V=D*(R+1);

disp(['Il valore assunto dalla portata di vapore V circolante in colonna � pari a ',num2str(V),' [kmol/h]'])

% Caso b) : xD = cost e R variabile
disp('   ')
disp('------ Caso b) : xD = cost e R variabile ------')
clear xD xDmedio R
xDm=0.9;
% Punto 1): calcolo del massimo valore assumibile da R
disp('Punto 1): calcolo del massimo valore assumibile da R')
R0=0.7;
Rmax=fzero('fbEs6bIC',R0);
disp(['Il valore assunto dalla massima frazione molare di benzene nel distillato � pari a ',num2str(Rmax)])

% Punto 2):
disp('   ')
disp('Punto 2): andamenti di R e Vtot in funzione di Dprimo fino a xB = 0.25')
Dprimoobb=52.32; % [kmol], � un dato preso dalla risoluzione della rettifica discontinua
% Precediamo ora mediante un procedimento di tipo iterativo, che contiene
% anche un calcolo numerico degli integrali presenti:
clear xD x4 x3 x2 xB y3 y2 y1 B Dprimo
R(1)=Rmax;
x4(1)=xDm/(alfa-xDm*(alfa-1));
y3(1)=(R(1)/(R(1)+1))*x4(1)+xDm/(R(1)+1);
x3(1)=y3(1)/(alfa-y3(1)*(alfa-1));
y2(1)=(R(1)/(R(1)+1))*x3(1)+xDm/(R(1)+1);
x2(1)=y2(1)/(alfa-y2(1)*(alfa-1));
y1(1)=(R(1)/(R(1)+1))*x2(1)+xDm/(R(1)+1);
xB(1)=y1(1)/(alfa-y1(1)*(alfa-1));
f1(1)=1/(xDm-xB(1));
int1(1)=0;
B(1)=M0;
Dprimo(1)=0;
f2(1)=0;
int2(1)=0;
Vtot(1)=0;
deltaR=0.00001;
i=1;
while xB(i)>0.25
   i=i+1;
   R(i)=R(i-1)+deltaR;
   x4(i)=xDm/(alfa-xDm*(alfa-1));
   y3(i)=(R(i)/(R(i)+1))*x4(i)+xDm/(R(i)+1);
   x3(i)=y3(i)/(alfa-y3(i)*(alfa-1));
   y2(i)=(R(i)/(R(i)+1))*x3(i)+xDm/(R(i)+1);
   x2(i)=y2(i)/(alfa-y2(i)*(alfa-1));
   y1(i)=(R(i)/(R(i)+1))*x2(i)+xDm/(R(i)+1);
   xB(i)=y1(i)/(alfa-y1(i)*(alfa-1));
   f1(i)=1/(xDm-xB(i));
   int1(i)=((f1(i)+f1(i-1))/2)*(xB(i)-xB(i-1));
   B(i)=B(i-1)*exp(int1(i));
   Dprimo(i)=M0-B(i);
   f2(i)=1/((xB(i)-xDm)^2*(1-R(i)/(R(i)+1)));
   int2(i)=((f2(i)+f2(i-1))/2)*(xB(i)-xB(i-1));
   Vtot(i)=Vtot(i-1)+M0*(z(1)-xDm)*int2(i);
end
disp('Si guardino i seguenti grafici')
subplot(1,2,1)
plot(Dprimo,R)
grid
xlabel('Dprimo [kmol]')
ylabel('R')
title('Grafico Dprimo-R')
subplot(1,2,2)
plot(Dprimo,Vtot)
grid
xlabel('Dprimo [kmol]')
ylabel('Vtot [kmol]')
title('Grafico Dprimo-Vtot')

