close all
clear all
clc

%Dati
Dtank = 12;
H = 7;
Dforo = 1.3;
Cd = 0.616;
Fi = 8.5;
h0 = 4.72;

%Calcolo stato stazionario
hss = (Fi/(Cd*pi/4*Dforo^2))^2/(2*9.81);
Lss = hss + 0.5; %N.B. --> altezza totale del liquido

%Integrazione ODE
hand = @(t,h)tank(t,h,Dtank,Fi,Cd,Dforo);
[t,h] = ode45(hand,[0 1000], h0);
L = zeros(length(h),1);
for i = 1:length(h)
    L(i) = h(i) + 0.5;
end

ss = zeros(1,length(t));  %N.B. --> usato solo per fare il grafico, per avere la retta ss,
for j = 1:length(ss)       %oppure faccio ones(length(h))*Lss
    ss(j) = Lss;
end

hold on
plot(t,L)
plot(t,ss,'--')


function hp = tank(~,h,Dtank,Fi,Cd,Dforo)
Aserb = pi/4*Dtank^2;
Fo = Cd*pi/4*Dforo^2*sqrt(2*9.81*h);

hp = (Fi-Fo)/Aserb;
end