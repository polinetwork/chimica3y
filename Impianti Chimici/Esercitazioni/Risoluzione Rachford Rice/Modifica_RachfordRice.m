
% Andrea Landella
% MSc Chemical Engineering, POLITECNICO DI MILANO

% Algoritmo di calcolo delle soluzioni per l'equazione di Rachford-Rice con
% riscrittura in termini polinomiali a coefficienti noti.
% Il calcolo dei coefficienti � dell'ordine di 2^N, dove N sono i composti
% in quanto entra in gioco il calcolo delle combinazioni C(N,k). Si ha
% quindi uno speedup immediato ed efficiente rispetto al metodo di azzera-
% mento classico della Rachford-Rice fino a circa 10-15 componenti.

clc
close all
clearvars

global NS zvec Kvec Cvec

s = rng(0);

% specie
NS = 15; 

% zin
zvec = rand(NS,1);
zvec = zvec/sum(zvec);

% K values
Kvec = rand(NS,1);

% vettore dei coefficienti
Cvec = calc_coeff(zvec,Kvec);

% trova le soluzioni noto il vettore dei coefficienti
tic
alf1 = roots(Cvec);
toc

% trova una soluzione
tic
alf2 = fzero(@calc_RR,0.5);
toc

% trova le alfa critiche
alf_crit = sort(1./(1-Kvec),'descend');

% figure
% fplot(@calc_RR,[-4 4],'r','LineWidth',1.2);
% hold on
% grid on
% fplot(@calc_RR_pol,[-4 4],'b','LineWidth',1.5);
% fplot(0,[-4 4],'-.k');
% for j=1:NS-1
%     plot(alf(j),0,'o','MarkerFaceColor','red','MarkerEdgeColor','red');
% end

%% Function declaration

% funzione di calcolo dei coefficienti
function f = calc_coeff(z,K)

    % z � vettore NS x 1, come K
    
    NS = numel(z);
    
    % vettore aux
    Y = K - 1;
    
    % vettore aux
    vec = 1:NS;
    
    % vettore dei coefficienti
    f = zeros(NS,1);
    %
    f(1) = 1;

    % calcolo dei coefficienti
    for k=2:NS

        % si scrive la matrice delle combinazioni
        CMAT = nchoosek(vec,k-1);

        % si scrivono le sue dimensioni    
        ROW = size(CMAT,1);
        COL = size(CMAT,2);

        for j=1:ROW      

            % inizializzo le parti
            z_sum = 0;
            Y_pro = 1;

            for l=1:COL
                z_sum = z_sum + z( CMAT(j,l) );
                Y_pro = Y_pro * Y( CMAT(j,l) );
            end

            f(k) = f(k) + (1-z_sum)/Y_pro;
        end   
    end
end

% funzione polinomiale
function f = calc_RR_pol(alf)

    global NS Cvec

    alf_vec = zeros(1,NS-1);
    
    for j=1:NS
        alf_vec(j) = alf.^(NS-j);
    end

    f = alf_vec*Cvec;
    
end

% funzione di rachford-rice
function f = calc_RR(alf)

    global NS zvec Kvec
    
    f = 0;
    
    for j=1:NS
        f = f + zvec(j)*(Kvec(j)-1)/(1+alf*(Kvec(j)-1));
    end
end













