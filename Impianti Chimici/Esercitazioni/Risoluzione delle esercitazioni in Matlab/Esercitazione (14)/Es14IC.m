%% Risoluzione dell'esercitazione (14) di Impianti Chimici: Ribollitore a termosifone verticale
clc
clear all
close all
disp('Ribollitore a termosifone verticale')
global Rec1 Rec2 Rec3 Rec4 Rec5
% Dati:
Pev=1.2; % [atm]
R=4; % Rapporto di ricircolazione (liquido/vapore massivo)
% Caratteristiche geometriche del ribollitore e tubazioni in accordo con la
% colonna
OD=(3/4)*2.54*1e-2; % [m]
s=0.065*2.54*1e-2; % [m]
k=45; % [W/(m K)]
Lt=12*0.3048; % [m] [ft]-->*0.3048-->[m]
ntubtir=203;
ntir=12;
ntub=ntubtir-ntir;
p=1*2.54*1e-2; % [m]
IDshell=17.25*2.54*1e-2; % [m]
Lc=8; % [m]
Dc=0.2; % [m]
Ac=pi*Dc^2/4; % [m^2]
ID=OD-2*s; % [m]
At=pi*ID^2/4; % [m^2]
beta=ntub*At/Ac;
epsilon=0.75;
% Propriet� chimico-fisiche del toluene:
PM(1)=92.141*1e-3; % [kg/mol]
Tc(1)=591.7; % [K]
Pc(1)=40.6; % [atm]
TNBP(1)=383.8; % [K]
DHevNBP(1)=7930*4.1858; % [J/mol]
C1=0.8488;
C2=0.26655;
C3=591.8;
C4=0.2878;
AntA(1)=16.0137;
AntB(1)=3096.52;
AntC(1)=-53.67;
B=467.33;
T0=255.24;
% Propriet� chimico-fisiche dell'acqua:
PM(2)=18.015*1e-3; % [kg/mol]
Tc(2)=647.3; % [K]
Pc(2)=217.6; % [atm]
TNBP(2)=373.2; % [K]
DHevNBP(2)=9717*4.1858; % [J/mol]
AntA(2)=18.3036;
AntB(2)=3816.44;
AntC(2)=-46.13;
% Coefficienti liminari di scambio:
hin=1700; % [W/(m^2 K)]
hext=5000; % [W/(m^2 K)]
g=9.81; % [m/s^2]

% Risoluzione:
% Dal momento che a fondo colonna abbiamo liquido saturo e conosciamo la
% sua tensione di vapore, possiamo determinare la temperatura del liquido:
Tliq=AntB(1)/(AntA(1)-log(Pev*760))-AntC(1);
lambda=1+(1-Tliq/C3)^C4;
roprimoliq=C1/(C2^lambda); % [kmol/m^3]
roliq=roprimoliq*(PM(1)*1e3); % [kg/m^3]
roprimovap=Pev*101325/(8.314*Tliq); % [mol/m^3]
rovap=roprimovap*PM(1); % [kg/m^3]
muliq=10^(B*(1/Tliq-1/T0))*1e-3; % [Pa s]
VB=1/roliq; % [m^3/kg]
Vvap=1/rovap; % [m^3/kg]
VAprimo=(1/(R+1))*Vvap+(R/(R+1))*VB;
romix=1/VAprimo; % [kg/m^3]
zB=0;
zA=Lt;

% Iniziamo ora con la procedura iterativa: Poniamo un valore iniziale della
% portata di toluene.
disp('   ')
disp('Punto 1): Determinazione della portata massiva di toluene elaborabile in condizioni di convezione naturale')
Wt(1)=20; % [kg/s]
Gc1=Wt(1)/Ac; % [kg/(m^2 s)]
Gt1=Wt(1)/(ntub*At);  % [kg/(m^2 s)]
Rec1=Gc1*Dc/muliq;
Ret1=Gt1*ID/muliq;
ft=0.0791*Ret1^(-1/4);
fc0=0.005;
fc=fzero('faEs14IC',fc0);
disp(['fc � pari a ',num2str(fc),])

Gtcalc2=(roliq*g*(zA-zB)-g*Lt/(VAprimo-VB)*log(VAprimo/VB))/((1/romix-1/roliq)+2*ft/ID*Lt*(VAprimo+VB)/2+1/(2*roliq)+1/(2*romix)+beta^2/(2*roliq)*(4*fc*Lc/Dc+2*epsilon));
Gtcalc=sqrt(Gtcalc2);
disp(['G2 � pari a ',num2str(Gtcalc2),])
disp(['Gt � pari a ',num2str(Gtcalc),])
Wt(2)=Gtcalc*At*ntub;
Gc2=Wt(2)/Ac; % [kg/(m^2 s)]
Gt2=Wt(2)/(ntub*At);  % [kg/(m^2 s)]
Rec2=Gc2*Dc/muliq;
Ret2=Gt2*ID/muliq;
ft=0.0791*Ret2^(-1/4);
fc0=0.005;
fc=fzero('fbEs14IC',fc0);
Gtcalc2=(roliq*g*(zA-zB)-g*Lt/(VAprimo-VB)*log(VAprimo/VB))/((1/romix-1/roliq)+2*ft/ID*Lt*(VAprimo+VB)/2+1/(2*roliq)+1/(2*romix)+beta^2/(2*roliq)*(4*fc*Lc/Dc+2*epsilon));
Gtcalc=sqrt(Gtcalc2);
Wt(3)=Gtcalc*At*ntub;
Gc3=Wt(3)/Ac; % [kg/(m^2 s)]
Gt3=Wt(3)/(ntub*At);  % [kg/(m^2 s)]
Rec3=Gc3*Dc/muliq;
Ret3=Gt3*ID/muliq;
ft=0.0791*Ret3^(-1/4);
fc0=0.005;
fc=fzero('fcEs14IC',fc0);

Gtcalc2=(roliq*g*(zA-zB)-g*Lt/(VAprimo-VB)*log(VAprimo/VB))/((1/romix-1/roliq)+2*ft/ID*Lt*(VAprimo+VB)/2+1/(2*roliq)+1/(2*romix)+beta^2/(2*roliq)*(4*fc*Lc/Dc+2*epsilon));
Gtcalc=sqrt(Gtcalc2);
Wt(4)=Gtcalc*At*ntub;
Gc4=Wt(4)/Ac; % [kg/(m^2 s)]
Gt4=Wt(4)/(ntub*At);  % [kg/(m^2 s)]
Rec4=Gc4*Dc/muliq;
Ret4=Gt4*ID/muliq;
ft=0.0791*Ret4^(-1/4);
disp(['Il valore da wt4 � pari a ',num2str(Wt(4)),' [kg/s]'])

fc0=0.005;
fc=fzero('fdEs14IC',fc0);
disp(['Il valore assunto da fc4 � pari a ',num2str(fc),' [kg/s]'])

Gtcalc2=(roliq*g*(zA-zB)-g*Lt/(VAprimo-VB)*log(VAprimo/VB))/((1/romix-1/roliq)+2*ft/ID*Lt*(VAprimo+VB)/2+1/(2*roliq)+1/(2*romix)+beta^2/(2*roliq)*(4*fc*Lc/Dc+2*epsilon));
Gtcalc=sqrt(Gtcalc2);
Wt(5)=Gtcalc*At*ntub;
Gc5=Wt(5)/Ac; % [kg/(m^2 s)]
Gt5=Wt(5)/(ntub*At);  % [kg/(m^2 s)]
Rec5=Gc5*Dc/muliq;
Ret5=Gt5*ID/muliq;
ft=0.0791*Ret5^(-1/4);
fc0=0.005;
fc=fzero('feEs14IC',fc0);
disp(['Il valore assunto da fc5 � pari a ',num2str(fc),' [kg/s]'])

Gtcalc2=(roliq*g*(zA-zB)-g*Lt/(VAprimo-VB)*log(VAprimo/VB))/((1/romix-1/roliq)+2*ft/ID*Lt*(VAprimo+VB)/2+1/(2*roliq)+1/(2*romix)+beta^2/(2*roliq)*(4*fc*Lc/Dc+2*epsilon));
Gtcalc=sqrt(Gtcalc2);
Wt(6)=Gtcalc*At*ntub;
disp(['Il valore assunto dalla portata massiva di toluene in condizioni di convezione naturale � pari a ',num2str(Wt(6)),' [kg/s]'])

disp('   ')
disp('Punto 2): Calcolo della pressione e della portata massiva del vapore necessario al servizio')
Wvap=Wt(6)/(R+1);
DHevTliq=(DHevNBP(1)*((1-Tliq/Tc(1))/(1-TNBP(1)/Tc(1)))^0.38)/(PM(1)*1e3); % [kJ/kg]
Q=Wvap*DHevTliq; % [kW]
Aex=ntub*pi*OD*Lt; % [m^2]
Ain=ntub*pi*ID*Lt; % [m^2]
Aml=(Aex-Ain)/log(Aex/Ain); % [m^2]
Uex=(Aex/(hin*Ain)+s/k*Aex/Aml+1/hext)^(-1); % [W/(m^2 K)]
Tvap=Tliq+Q*1e3/(Uex*Aex);
Pvap=exp(AntA(2)-AntB(2)/(Tvap+AntC(2)))/760; % [atm]
disp(['Il valore assunto dalla pressione del vapore � pari a ',num2str(Pvap),' [atm]'])
DHevTvap=(DHevNBP(2)*((1-Tvap/Tc(2))/(1-TNBP(2)/Tc(2)))^0.38)/(PM(2)*1e3); % [J/kg]
WH2Ovap=Q/DHevTvap; % [kg/s]
disp(['Il valore assunto dalla portata massiva di vapore acqueo � pari a ',num2str(WH2Ovap),' [kg/s]'])