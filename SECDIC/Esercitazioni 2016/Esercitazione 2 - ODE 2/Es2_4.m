clear all
clc

global p

psi=[0.35:0.1:0.65];

for i=1:length(psi)
p=psi(i);
[t,x]=ode45('sistemaODEes_4',[0:0.001:100],[0 1]);
z(:,i)=x(:,1);
teta(:,i)=x(:,2);

subplot(1,2,1)
plot(t,z(:,i),'g');
grid on
xlabel('t'); ylabel('z');
hold on

subplot(1,2,2)
plot(t,teta(:,i),'b');
grid on
xlabel('t'); ylabel('teta');
hold on
axis([0 30 0 12])
end
