function dhdt=fEx1Es5(t,h)

global Fin Cd Aforo g Abase

Fout=Cd*Aforo*sqrt(2*g*h);

dhdt=(Fin-Fout)/Abase;