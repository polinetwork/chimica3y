%% Risoluzione dell'esercitazione (12) di Impianti Chimici: Verifica di uno scambiatore a tubi concentrici
clc
clear all
close all
disp('Verifica di uno scambiatore a tubi concentrici')
% Dati: Il fluido caldo corrisponde all'idrocarburo, mentre il fluido
% freddo corrisponde all'acqua. Vogliamo che l'idrocarburo esca ad una
% temperatura compresa tra 40 e 45 [�C]. I dati in forma vettoriali si
% riferiscono al vettore S=[idrocarburo acqua]; Lo scambiatore �
% controcorrente
Whc=0.95; % [kg/s]
ro=[838 996]; % [kg/m^3]
cp=[1884 4181]; % [J/(kg K)]
k=[0.148 0.616]; % [W/(m K)]
vH2O=1.5; % [m/s], per evitare condizioni stagnanti
Tin=82; % [�C]
tin=21; % [�C]
kacc=45; % [W/(m K)]
mu1=0.4863; % [cP], relativo ad una temperatura di 40 [�C]
mu2=0.3821; % [cP], relativo ad una temperatura di 60 [�C]
T1=40+273.15; % [K]
T2=60+273.15; % [K]
IDint=33.884e-3; % [m]
IDest=45.974e-3; % [m]
sint=2.108e-3; % [m]
T0(2)=293.16; % [K]
B(2)=658.25;
L=25; % [m]
Rhc=0.0002; % [(m^2 K)/W]
RH2O=0.0002; % [(m^2 K)/W]
% Risoluzione:
T0(1)=(1-log(mu1)/log(mu2))/(1/T1-1/T2*log(mu1)/log(mu2));
B(1)=log10(mu2)/(1/T2-1/T0(1));
Ahc=pi*IDint^2/4;
vhc=Whc/(ro(1)*Ahc);
ODint=IDint+2*sint; % [m]
AH2O=(pi*IDest^2/4-pi*ODint^2/4); % [m^2]
WH2O=vH2O*ro(2)*AH2O; % [kg/s]

%% Prima iterazione:
disp('   ')
disp('---------- Prima iterazione ----------')
% Nella prima iterazione:
% 1) Consideriamo la temperatura media al posto di quella calorica
% 2) Trascuriamo le correzioni sulla viscosit�
% 3) Trascuriamo i fattori di sporcamento
% 4) Poniamo Tout=40 [�C]
Tout=40; % [�C]
tout=Whc*cp(1)*(Tin-Tout)/(WH2O*cp(2))+tin; % [�C]
% Effettuiamo i calcoli relativi all'idrocarburo:
Tmediahc=((Tin+273.15)+(Tout+273.15))/2; % [K]
muhc=10^(B(1)*(1/Tmediahc-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
Deq(1)=4*Ahc/(pi*IDint);
ReD(1)=ro(1)*vhc*Deq(1)/muhc;
Pr(1)=muhc*cp(1)/k(1);
Nu(1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3);
hi=Nu(1)*k(1)/Deq(1); % [W/(m^2 K)]
% Effettuiamo i calcoli relativi all'acqua:
tmediaH2O=((tin+273.15)+(tout+273.15))/2;
Deq(2)=4*AH2O/(pi*ODint);
muH2O=(10^(B(2)*(1/tmediaH2O-1/T0(2))))*1e-3;
ReD(2)=ro(2)*vH2O*Deq(2)/(muH2O);
Pr(2)=muH2O*cp(2)/k(2);
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3);
he=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
Ai=pi*L*IDint; % [m^2]
Ae=pi*L*ODint; % [m^2]
Aln=(Ae-Ai)/log(Ae/Ai); % [m^2]
UA=(1/(hi*Ai)+1/(he*Ae)+sint/(kacc*Aln))^(-1); % [W/K]
% Applichiamo ora il metodo dell'efficienza:
WcpH2O=WH2O*cp(2);
Wcphc=Whc*cp(1);
if WcpH2O<Wcphc
    Wcpmin=WcpH2O;
    Wcpmax=Wcphc;
    R=(WcpH2O/Wcphc);
else
    Wcpmax=WcpH2O;
    Wcpmin=Wcphc;
    R=(Wcphc/WcpH2O);
end
NUT=UA/Wcpmin;
etacontr=(1-(exp(-NUT*(1-R))))/(1-(R*exp(-NUT*(1-R))));
Tout=Tin-etacontr*(Tin-tin)+273.15; % [K]
tout=tin+R*(Tin+273.15-Tout)+273.15; % [K]
disp(['Il valore assunto da UA � pari a ',num2str(UA),' [W/K]'])
disp(['Il valore assunto da NUT � pari a ',num2str(NUT)])
disp(['Il valore assunto dal rendimento � pari a ',num2str(etacontr)])
disp(['Il valore assunto da Tout � pari a ',num2str(Tout),' [K]'])
disp(['Il valore assunto da tout � pari a ',num2str(tout),' [K]'])
%% Seconda iterazione: 
disp('   ')
disp('---------- Seconda iterazione ----------')
% Nella seconda iterazione:
% 1) Consideriamo le T caloriche
% 2) Consideriamo i fattori di sporcamento
% 3) Trascuriamo le correzioni relative alla viscosit�

% Consideriamo ora il lato caldo:
Tin=82+273.15; % [K]
muhc=10^(B(1)*(1/Tin-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
Deq(1)=4*Ahc/(pi*IDint);
ReD(1)=ro(1)*vhc*Deq(1)/muhc;
Pr(1)=muhc*cp(1)/k(1);
Nu(1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3);
hicaldo=Nu(1)*k(1)/Deq(1); % [W/(m^2 K)]
Deq(2)=4*AH2O/(pi*ODint);
muH2O=(10^(B(2)*(1/tout-1/T0(2))))*1e-3;
ReD(2)=ro(2)*vH2O*Deq(2)/(muH2O);
Pr(2)=muH2O*cp(2)/k(2);
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3);
hecaldo=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
Ai=pi*L*IDint; % [m^2]
Ae=pi*L*ODint; % [m^2]
Aln=(Ae-Ai)/log(Ae/Ai); % [m^2]
Uestcaldo=(Ae/(hicaldo*Ai)+1/hecaldo+sint*Ae/(kacc*Aln)+Rhc+RH2O)^(-1); % [W/(m^2 K)]
disp(['Il valore assunto da Uestcaldo � pari a ',num2str(Uestcaldo),' [W/(m^2 K)]'])

% Consideriamo ora il lato freddo:
tin=294.15; % [K]
muhc=10^(B(1)*(1/Tout-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
Deq(1)=4*Ahc/(pi*IDint);
ReD(1)=ro(1)*vhc*Deq(1)/muhc;
Pr(1)=muhc*cp(1)/k(1);
Nu(1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3);
hifreddo=Nu(1)*k(1)/Deq(1); % [W/(m^2 K)]
Deq(2)=4*AH2O/(pi*ODint);
muH2O=(10^(B(2)*(1/tin-1/T0(2))))*1e-3;
ReD(2)=ro(2)*vH2O*Deq(2)/(muH2O);
Pr(2)=muH2O*cp(2)/k(2);
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3);
hefreddo=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
Ai=pi*L*IDint; % [m^2]
Ae=pi*L*ODint; % [m^2]
Aln=(Ae-Ai)/log(Ae/Ai); % [m^2]
Uestfreddo=(Ae/(hifreddo*Ai)+1/hefreddo+sint*Ae/(kacc*Aln)+Rhc+RH2O)^(-1); % [W/(m^2 K)]
disp(['Il valore assunto da Uestfreddo � pari a ',num2str(Uestfreddo),' [W/(m^2 K)]'])

% Proseguiamo ora con il calcolo delle T caloriche:
r=(Tout-tin)/(Tin-tout);
Kc=(Uestcaldo-Uestfreddo)/Uestfreddo;
Fcmedio=(1/Kc+r/(r-1))/(1+log(Kc+1)/log(r))-1/Kc;
Tcalhc=Tout+Fcmedio*(Tin-Tout); % [K]
tcalH2O=tin+Fcmedio*(tout-tin); % [K]
disp(['Il valore assunto dalla T calorica dello hc � pari a ',num2str(Tcalhc),' [K]'])
disp(['Il valore assunto dalla T calorica della H2O � pari a ',num2str(tcalH2O),' [K]'])

% Ora che abbiamo determinato le T caloriche, procediamo in modo analogo
% alla prima iterazione:
% Effettuiamo i calcoli relativi all'idrocarburo:
muhc=10^(B(1)*(1/Tcalhc-1/T0(1)))*1e-3;% [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
disp(['Il valore assunto da muhccal � pari a ',num2str(muhc),' [W/(m^2 K)]'])

Deq(1)=4*Ahc/(pi*IDint);
ReD(1)=ro(1)*vhc*Deq(1)/muhc;
Pr(1)=muhc*cp(1)/k(1);
Nu(1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3);
hi=Nu(1)*k(1)/Deq(1); % [W/(m^2 K)]
% Effettuiamo i calcoli relativi all'acqua:
Deq(2)=4*AH2O/(pi*ODint);
muH2O=(10^(B(2)*(1/tcalH2O-1/T0(2))))*1e-3;
disp(['Il valore assunto da muh2o � pari a ',num2str(muH2O),' [W/(m^2 K)]'])

ReD(2)=ro(2)*vH2O*Deq(2)/(muH2O);
Pr(2)=muH2O*cp(2)/k(2);
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3);
disp(['Il valore assunto da nu2 � pari a ',num2str(Nu(2)),' [W/(m^2 K)]'])

he=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
Ai=pi*L*IDint; % [m^2]
Ae=pi*L*ODint; % [m^2]
Aln=(Ae-Ai)/log(Ae/Ai); % [m^2]
Uesteff=(Ae/(hi*Ai)+1/he+sint*Ae/(kacc*Aln)+Rhc+RH2O)^(-1); % [W/(m^2 K)]
UAest=Uesteff*Ae;
% Applichiamo ora il metodo dell'efficienza:
NUT=UAest/Wcpmin;
etacontr=(1-(exp(-NUT*(1-R))))/(1-(R*exp(-NUT*(1-R))));
Tout=Tin-etacontr*(Tin-tin); % [K]
tout=tin+R*(Tin-Tout); % [K]
disp(['Il valore assunto da Uesteff � pari a ',num2str(Uesteff),' [W/(m^2 K)]'])
disp(['Il valore assunto da NUT � pari a ',num2str(NUT)])
disp(['Il valore assunto dal rendimento � pari a ',num2str(etacontr)])
disp(['Il valore assunto da Tout � pari a ',num2str(Tout),' [K]'])
disp(['Il valore assunto da tout � pari a ',num2str(tout),' [K]'])

%% Terza iterazione: 
disp('   ')
disp('---------- Terza iterazione ----------')
% Nella terza iterazione teniamo conto di tutti i fattori che cerchino di
% descrivere al meglio il comportamento reale dello scambiatore di calore:

% Consideriamo ora il lato caldo:
Tin=82+273.15; % [K]
muhcb=10^(B(1)*(1/Tin-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
disp(['Il valore assunto da muhcb � pari a ',num2str(muhcb),' [K]'])
Deq(1)=4*Ahc/(pi*IDint);
ReD(1)=ro(1)*vhc*Deq(1)/muhcb;
Pr(1)=muhcb*cp(1)/k(1);
Nu(1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3);
hicaldo(1)=Nu(1)*k(1)/Deq(1); % [W/(m^2 K)]
Deq(2)=4*AH2O/(pi*ODint);
muH2Ob=(10^(B(2)*(1/tout-1/T0(2))))*1e-3;
ReD(2)=ro(2)*vH2O*Deq(2)/(muH2Ob);
Pr(2)=muH2Ob*cp(2)/k(2);
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3);
hecaldo(1)=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
disp(['Il valore assunto da he2 � pari a ',num2str(hecaldo(1)),' [K]'])

% Possiamo ora determinare la temperatura di parete:
Tw(1)=(Ai*hicaldo(1)*Tin+Ae*hecaldo(1)*tout)/(Ai*hicaldo(1)+Ae*hecaldo(1));
% Conoscendo la temperatura di parete, possiamo determinare la viscosit�
% dinamica dei fluidi alle condizioni di parete:
muhcp(1)=10^(B(1)*(1/Tw(1)-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale

disp(['Il valore assunto da muhcp � pari a ',num2str(muhcp(1)),' [K]'])
muH2Op(1)=(10^(B(2)*(1/Tw(1)-1/T0(2))))*1e-3;
Nu(2)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp(1))^0.14;
hicaldo(2)=Nu(2)*k(1)/Deq(1); % [W/(m^2 K)]
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op(1))^0.14;
hecaldo(2)=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
Tw(2)=(Ai*hicaldo(2)*Tin+Ae*hecaldo(2)*tout)/(Ai*hicaldo(2)+Ae*hecaldo(2));
i=2;
while abs(Tw(i)-Tw(i-1))>1e-10
muhcp(i)=10^(B(1)*(1/Tw(i)-1/T0(1)))*1e-3; 
muH2Op(i)=(10^(B(2)*(1/Tw(i)-1/T0(2))))*1e-3;
Nu(i+1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp(i))^0.14;
hicaldo(i+1)=Nu(i+1)*k(1)/Deq(1); % [W/(m^2 K)]
Nu(i+1)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op(i))^0.14;
hecaldo(i+1)=Nu(i+1)*k(2)/Deq(2); % [W/(m^2 K)]
Tw(i+1)=(Ai*hicaldo(i+1)*Tin+Ae*hecaldo(i+1)*tout)/(Ai*hicaldo(i+1)+Ae*hecaldo(i+1));
i=i+1;
end

Tw=Tw(i-1);
muhcp=10^(B(1)*(1/Tw-1/T0(1)))*1e-3; 
muH2Op=(10^(B(2)*(1/Tw-1/T0(2))))*1e-3;
Nu=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp)^0.14;
hicaldo=Nu*k(1)/Deq(1); % [W/(m^2 K)]
Nu=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op)^0.14;
hecaldo=Nu*k(2)/Deq(2); % [W/(m^2 K)]
Uestcaldo=(Ae/(hicaldo*Ai)+1/hecaldo+sint*Ae/(kacc*Aln)+Rhc+RH2O)^(-1); % [W/(m^2 K)]
disp(['Il valore assunto da Uestcaldo � pari a ',num2str(Uestcaldo),' [W/(m^2 K)]'])
disp(['Il valore assunto dalla Tw al lato caldo � pari a ',num2str(Tw),' [K]'])

% Ripetiamo la stessa procedura per il lato freddo:
% Consideriamo ora il lato freddo:
muhcb=10^(B(1)*(1/Tout-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
Deq(1)=4*Ahc/(pi*IDint);
ReD(1)=ro(1)*vhc*Deq(1)/muhcb;
Pr(1)=muhcb*cp(1)/k(1);
Nu(1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3);
hifreddo(1)=Nu(1)*k(1)/Deq(1); % [W/(m^2 K)]
Deq(2)=4*AH2O/(pi*ODint);
muH2Ob=(10^(B(2)*(1/tin-1/T0(2))))*1e-3;
ReD(2)=ro(2)*vH2O*Deq(2)/(muH2Ob);
Pr(2)=muH2Ob*cp(2)/k(2);
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3);
hefreddo(1)=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]

% Possiamo ora determinare la temperatura di parete:
Tw(1)=(Ai*hifreddo(1)*Tout+Ae*hefreddo(1)*tin)/(Ai*hifreddo(1)+Ae*hefreddo(1));
% Conoscendo la temperatura di parete, possiamo determinare la viscosit�
% dinamica dei fluidi alle condizioni di parete:
muhcp(1)=10^(B(1)*(1/Tw(1)-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
muH2Op(1)=(10^(B(2)*(1/Tw(1)-1/T0(2))))*1e-3;
Nu(2)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp(1))^0.14;
hifreddo(2)=Nu(2)*k(1)/Deq(1); % [W/(m^2 K)]
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op(1))^0.14;
hefreddo(2)=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
Tw(2)=(Ai*hifreddo(2)*Tout+Ae*hefreddo(2)*tin)/(Ai*hifreddo(2)+Ae*hefreddo(2));
i=2;
while abs(Tw(i)-Tw(i-1))>1e-10
muhcp(i)=10^(B(1)*(1/Tw(i)-1/T0(1)))*1e-3; 
muH2Op(i)=(10^(B(2)*(1/Tw(i)-1/T0(2))))*1e-3;
Nu(i+1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp(i))^0.14;
hifreddo(i+1)=Nu(i+1)*k(1)/Deq(1); % [W/(m^2 K)]
Nu(i+1)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op(i))^0.14;
hefreddo(i+1)=Nu(i+1)*k(2)/Deq(2); % [W/(m^2 K)]
Tw(i+1)=(Ai*hifreddo(i+1)*Tout+Ae*hefreddo(i+1)*tin)/(Ai*hifreddo(i+1)+Ae*hefreddo(i+1));
i=i+1;
end

Tw=Tw(i-1);
muhcp=10^(B(1)*(1/Tw-1/T0(1)))*1e-3; 
muH2Op=(10^(B(2)*(1/Tw-1/T0(2))))*1e-3;
Nu=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp)^0.14;
hifreddo=Nu*k(1)/Deq(1); % [W/(m^2 K)]
Nu=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op)^0.14;
hefreddo=Nu*k(2)/Deq(2); % [W/(m^2 K)]
Uestfreddo=(Ae/(hifreddo*Ai)+1/hefreddo+sint*Ae/(kacc*Aln)+Rhc+RH2O)^(-1); % [W/(m^2 K)]
disp(['Il valore assunto da Uestfreddo � pari a ',num2str(Uestfreddo),' [W/(m^2 K)]'])
disp(['Il valore assunto dalla Tw al lato freddo � pari a ',num2str(Tw),' [K]'])


% Proseguiamo ora con il calcolo delle T caloriche:
r=(Tout-tin)/(Tin-tout);
Kc=(Uestcaldo-Uestfreddo)/Uestfreddo;
Fcmedio=(1/Kc+r/(r-1))/(1+log(Kc+1)/log(r))-1/Kc;
Tcalhc=Tout+Fcmedio*(Tin-Tout); % [K]
tcalH2O=tin+Fcmedio*(tout-tin); % [K]
disp(['Il valore assunto dalla T calorica dello hc � pari a ',num2str(Tcalhc),' [K]'])
disp(['Il valore assunto dalla T calorica della H2O � pari a ',num2str(tcalH2O),' [K]'])

% Possiamo ora procedere con le medesime considerazioni sulla Tw,
% effettuate per� con le temperature caloriche:
% Consideriamo ora il lato freddo:
muhcb=10^(B(1)*(1/Tcalhc-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
Deq(1)=4*Ahc/(pi*IDint);
ReD(1)=ro(1)*vhc*Deq(1)/muhcb;
Pr(1)=muhcb*cp(1)/k(1);
Nu(1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3);
hical(1)=Nu(1)*k(1)/Deq(1); % [W/(m^2 K)]
Deq(2)=4*AH2O/(pi*ODint);
muH2Ob=(10^(B(2)*(1/tcalH2O-1/T0(2))))*1e-3;
ReD(2)=ro(2)*vH2O*Deq(2)/(muH2Ob);
Pr(2)=muH2Ob*cp(2)/k(2);
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3);
hecal(1)=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]

% Possiamo ora determinare la temperatura di parete:
Tw(1)=(Ai*hical(1)*Tcalhc+Ae*hecal(1)*tcalH2O)/(Ai*hical(1)+Ae*hecal(1));
% Conoscendo la temperatura di parete, possiamo determinare la viscosit�
% dinamica dei fluidi alle condizioni di parete:
muhcp(1)=10^(B(1)*(1/Tw(1)-1/T0(1)))*1e-3; % [kg/(m s)], la moltiplicazione per 1e-3 serve a convertire da cP all'unit� di misura del sistema internazionale
muH2Op(1)=(10^(B(2)*(1/Tw(1)-1/T0(2))))*1e-3;
Nu(2)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp(1))^0.14;
hical(2)=Nu(2)*k(1)/Deq(1); % [W/(m^2 K)]
Nu(2)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op(1))^0.14;
hecal(2)=Nu(2)*k(2)/Deq(2); % [W/(m^2 K)]
Tw(2)=(Ai*hical(2)*Tcalhc+Ae*hecal(2)*tcalH2O)/(Ai*hical(2)+Ae*hecal(2));
i=2;
while abs(Tw(i)-Tw(i-1))>1e-10
muhcp(i)=10^(B(1)*(1/Tw(i)-1/T0(1)))*1e-3; 
muH2Op(i)=(10^(B(2)*(1/Tw(i)-1/T0(2))))*1e-3;
Nu(i+1)=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp(i))^0.14;
hical(i+1)=Nu(i+1)*k(1)/Deq(1); % [W/(m^2 K)]
Nu(i+1)=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op(i))^0.14;
hecal(i+1)=Nu(i+1)*k(2)/Deq(2); % [W/(m^2 K)]
Tw(i+1)=(Ai*hical(i+1)*Tcalhc+Ae*hecal(i+1)*tcalH2O)/(Ai*hical(i+1)+Ae*hecal(i+1));
i=i+1;
end

Tw=Tw(i-1);
muhcp=10^(B(1)*(1/Tw-1/T0(1)))*1e-3; 
muH2Op=(10^(B(2)*(1/Tw-1/T0(2))))*1e-3;
Nu=0.023*ReD(1)^(4/5)*Pr(1)^(1/3)*(muhcb/muhcp)^0.14;
hical=Nu*k(1)/Deq(1); % [W/(m^2 K)]
Nu=0.023*ReD(2)^(4/5)*Pr(2)^(1/3)*(muH2Ob/muH2Op)^0.14;
hecal=Nu*k(2)/Deq(2); % [W/(m^2 K)]
Uesteff=(Ae/(hical*Ai)+1/hecal+sint*Ae/(kacc*Aln)+Rhc+RH2O)^(-1); % [W/(m^2 K)]
disp(['Il valore assunto da Uesteff � pari a ',num2str(Uesteff),' [W/(m^2 K)]'])
disp(['Il valore assunto dalla Tw con le T caloriche � pari a ',num2str(Tw),' [K]'])
UAest=Uesteff*Ae;
% Applichiamo ora il metodo dell'efficienza:
NUT=UAest/Wcpmin;
etacontr=(1-(exp(-NUT*(1-R))))/(1-(R*exp(-NUT*(1-R))));
Tout=Tin-etacontr*(Tin-tin); % [K]
tout=tin+R*(Tin-Tout); % [K]
disp(['Il valore assunto da NUT � pari a ',num2str(NUT)])
disp(['Il valore assunto dal rendimento � pari a ',num2str(etacontr)])
disp(['Il valore assunto da Tout � pari a ',num2str(Tout-273.15),' [�C]'])
disp(['Il valore assunto da tout � pari a ',num2str(tout-273.15),' [�C]'])
Q=Wcphc*(Tin-Tout);
disp(['Il valore della potenza termica scambiata tra i due fluidi nello scambiatore � ',num2str(Q),' [W]'])