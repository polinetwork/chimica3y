clear all
clc
format short

global A Ad Cd g Fino span H

H=7;
D=12;
A=pi*D^2/4;
g=9.80665;
d=1.3;
Ad=pi*d^2/4;
Cd=0.61;
hsotto=0.5;
Fino=8.5; %m3/s

hin=4.72;

span=1800;
[t,h]=ode45('ODE',[0 5*span],hin-hsotto);
h=h+hsotto;
plot(t/60,h,'-o')
grid on
xlabel('t [min]'); 
ylabel('h [m]');