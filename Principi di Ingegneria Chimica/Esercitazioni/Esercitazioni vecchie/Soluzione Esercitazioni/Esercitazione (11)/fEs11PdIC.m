function F=fEs11PdIC(teta)

global k Keq beta gamma radice tm epsilon

F=((beta-radice)*(radice*k*tm/Keq)*epsilon*exp((radice*k*tm/Keq)*teta)*((2*gamma-2*gamma*epsilon*exp((radice*k*tm/Keq)*teta))*(teta+1)))-(-(beta+radice)+(beta-radice)*epsilon*exp((radice*k*tm/Keq)*teta))*((2*gamma-2*gamma*epsilon*exp((radice*k*tm/Keq)*teta))+(teta+1)*(-2*gamma*epsilon*radice/Keq*k*tm*exp((radice*k*tm/Keq)*teta)));
