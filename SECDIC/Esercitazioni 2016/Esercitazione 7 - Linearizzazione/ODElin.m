function f=ODElin(t,h)

global A csi ho

if h>=0
    f=-csi*(sqrt(ho)+(1/(2*ho))*(h-ho))/A;
else
    f=0;    
end
end