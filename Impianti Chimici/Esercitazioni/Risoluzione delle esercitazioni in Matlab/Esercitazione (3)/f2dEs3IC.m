function F=f2dEs3IC(Rmin)

global alfa xD z

F=Rmin-(1/(alfa-1))*(xD(1)/z(1)-alfa*(xD(2)/z(2)));