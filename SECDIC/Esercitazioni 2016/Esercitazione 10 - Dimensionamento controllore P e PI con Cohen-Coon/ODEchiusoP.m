function f=ODEchiusoP(t,y)

global Fino A1 A2 R1 Kc h2sp span

h1=y(1);
h2=y(2);
F1=(h1)/R1;

if t<2*span
    F2=1.43*h2;
    Fin=Fino;
else
    F2=1.9*h2;
    Fin=Fino+Kc*(h2sp-h2); 
end    
    
if Fin<0
    Fin=0;
end

f(1)=(Fin-F1)/A1;
f(2)=(F1-F2)/A2;
f=f';

