%Caso 1

close all
clear all
clc

%Dati
D = 8; %[m]
H = 6; %[m]
Fi = 10/60; %[m3/s]
Ti = 60; %[�C]
Fo = 10/60; %[m3/s]
L0 = 2.5; %[m]
T0 = 60; %[�C]
ro = 1000; %[kg/m3]
cp = 4186; %[J/kg/K]
Q = 0; %[W]

hand = @(t,y)serb(t,y,Fi,Fo,D,ro,cp,Ti,Q);
[t,y] = ode113(hand,[0 50*60],[L0 T0]);

figure(1)
plot(t,y(:,1))
title('Livello')
xlabel('t[s]')
ylabel('L[m]')
figure(2)
plot(t,y(:,2))
title('Temperatura')
xlabel('t[s]')
ylabel('T[�C]')


function yp = serb(t,y,Fi,Fo,D,ro,cp,Ti,Q)   %y(1) � il livello, y(2) � la temperatura (uscente), uguale per le equazioni

if (t >= 10*60) && (t < 20*60)                 %N.B. --> scrivo solo le condizioni che mi interessano, le altre non le scrivo
    Fo = 0;
elseif (t >= 25*60) && (t < (25+18)*60)
    Fi = 5/60;
end

yp(1,1) = (Fi-Fo)/(D^2*pi/4);
yp(2,1) = (ro*cp*Fi*(Ti-y(2))+Q)/(ro*pi/4*D^2*y(1)*cp);

end