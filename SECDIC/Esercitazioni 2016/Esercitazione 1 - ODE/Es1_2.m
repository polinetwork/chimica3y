function main
    close all
    clear all
    clc
    
    global k1 k2
    
    k1=0.5; k2=0.3;
    
    [t,c]=ode23(@sistemaODE,[0 10],[3 0 0]);
    plot(t,c(:,1),'r',t,c(:,2),'b',t,c(:,3),'g');
    grid on
    
    cBmax=max(c(:,2))
    tcBmax=t(find(c(:,2)==cBmax))
end

function f=sistemaODE(t,c)

global k1 k2

R=[k1*c(1) k2*c(2)];

f=[-R(1);
    R(1)-R(2);
    R(2)];
end