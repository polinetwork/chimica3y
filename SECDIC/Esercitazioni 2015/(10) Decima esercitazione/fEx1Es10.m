function dhdt=fEx1Es10(t,h)

global Fin Cd Af g A

Fout=Cd*Af*sqrt(2*g*h);

dhdt=(Fin-Fout)/A;