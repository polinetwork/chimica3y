clear all
clc

global K phi phio P Po v

%etanolo etilene H2O

Tc=[513.9 282.3 647.1];
Pc=[61.48 50.4 220.55]; %bar
om=[0.645 0.087 0.345];

Dgf=[-168.5 68.5 -228.6]*1e3;
Dhf=[-235.1 52.5 -241.8]*1e3;

v=[1 -1 -1];
T=473;
P=34.5; %bar
Po=[30.22 0 15.55]; %bar
    
Dgo=Dgf*v';
Dh=Dhf*v'; %costante
Ko=exp(-Dgo/(8.314*298));

K=Ko*exp( quad(@(t) Dh./(8.314*t.^2),298,T) )

Bo=0.083-0.422./(T./Tc).^1.6
B1=0.139-0.172./(T./Tc).^4.2

phi=exp( (Bo+om.*B1).*(P./Pc)./(T./Tc) )
phio=exp( (Bo+om.*B1).*(Po./Pc)./(T./Tc) )

Xo=[0.3 0.3 0.5];
X=fsolve('sistema',Xo);
y=[X(1) X(2) 1-X(1)-X(2)]
x=[X(3) 0 1-X(3)]


