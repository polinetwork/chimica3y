%Calcolo potenza termica istantanea

close all
clear all
clc

%Dati (sovrabbondanti)
D = 8; %[m]
H = 6; %[m]
Fi = 10/60; %[m3/s]
Ti = 60; %[�C]
Fo = 10/60; %[m3/s]
L0 = 2.5; %[m]
T0 = 60; %[�C]
ro = 1000; %[kg/m3]
cp = 4186; %[J/kg/K]
U = 100; %[W/m2/K]

hand = @(t,L)serb(t,L,Fi,Fo,D);
[t,L] = ode113(hand,[0 50*60],L0);

Q = zeros(1,length(t));             %a ogni istante calcolo la potenza da fornire (uguale a quella dispersa)
for i = 1:length(t)
    Q(i) = U*pi*D*L(i)*(Ti-25)/1000; %[kW]
end

figure(1)
plot(t,L)
title('Livello')
xlabel('t[s]')
ylabel('L[m]')
figure(2)
plot(t,Q)
title('Potenza termica')
xlabel('t[s]')
ylabel('Q[kW]')


function Lp = serb(t,~,Fi,Fo,D)   %calcolo solo il livello, Q nel main

if (t >= 10*60) && (t < 20*60)                 %N.B. --> scrivo solo le condizioni che mi interessano, le altre non le scrivo
    Fo = 0;
elseif (t >= 25*60) && (t < (25+18)*60)
    Fi = 5/60;
end

Lp = (Fi-Fo)/(D^2*pi/4);

end