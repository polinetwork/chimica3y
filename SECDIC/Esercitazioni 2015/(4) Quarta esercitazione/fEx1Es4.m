function dTdt=fEx1Es4(t,T)
global Fin D L Tin U ro cp Tamb
dTdt=(ro*Fin*cp*(Tin-T)-U*pi*D*L*(T-Tamb))/(ro*pi*D^2/4*L*cp);