%Simulazione controllore PI

close all
clear all
clc

%Dati
Fi = 9.4;
A1 = 30;
r = 1.2;
A2 = 50;
h0 = 3;
hsp = 6.6;
kc = -30;
tauI = 10;

%Integrazione
hand = @(t,h)tanks(t,h,A1,A2,Fi,r,hsp,kc,tauI);
hand_int = @(t,h,flag)integrale(t,h,flag,hsp);
opt = odeset('RelTol',1e-8,'OutputFcn',hand_int,'Refine',1);  %devo usare Refine=1
[t,h] = ode45(hand,[0 60*10],[h0 h0],opt);

hspplot = zeros(1,length(t));
for i = 1:length(t)
    hspplot(i) = hsp;
end
hold on
plot(t,h(:,1))
plot(t,h(:,2))
plot(t,hspplot,'--')
legend('serb. 1','serb. 2','set point')

function hp = tanks(t,h,A1,A2,Fi,r,hsp,kc,tauI)
global int t_old eps_old

if t > 5*60
    Fi = 2*Fi;
end

F1 = (h(1)-h(2))/r;
eps = hsp-h(2);
if isempty(int)           %N.B. --> dato che int non esiste all'inizio, quello locale lo gestisco manualmente
    int_loc = 0;
else
    int_loc = (eps+eps_old)/2*(t-t_old) + int; %N.B. --> devo aggiornarlo, con l'ultimo tempo (quello attuale, perch� lui aveva chimato int prima??)
end
 Fo = 9.4 + kc*(hsp-h(2)) + kc/tauI*int_loc;          %cambia Fo
 Fo = max(0,Fo); %limiti

hp(1,1) = (Fi-F1)/A1;
hp(2,1) = (F1-Fo)/A2;

end

function status = integrale(t,h,flag,hsp)
global int eps_old t_old
status = 0;

if strcmp('init',flag)
    int = 0;
    t_old = 0;
    eps_old = 0;
elseif strcmp('',flag)
    
    eps = hsp-h(2); %eps step corrente

    int = (eps+eps_old)/2*(t-t_old) + int;  % <-- qui aggiungo un pezzo alla volta, con i trapezi

    t_old = t;
    eps_old = eps; %cio� li aggiorno ogni volta
end

end