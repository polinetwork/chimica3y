function f=sistemaODEes_4(t,x)

global p

z=x(1);
teta=x(2);

n=1; B=20; eps=0.05;
h=exp(teta/(1+eps*teta));

f(1)=(p/B)*(1-z)^n*h;
f(2)=p*(1-z)^n*h-teta;
f=f';