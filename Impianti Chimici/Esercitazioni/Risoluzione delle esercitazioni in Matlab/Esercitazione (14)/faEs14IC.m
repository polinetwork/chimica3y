function F=faEs14IC(fc)

global Rec1

F=1/sqrt(fc)-4*log10(Rec1*sqrt(fc))+0.4;