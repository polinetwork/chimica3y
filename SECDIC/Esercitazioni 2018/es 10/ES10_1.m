%Serbatoio riscaldato
close all
clear all
clc

global Fvvett

%Dati
V = 5; %[m3]
t_res = 2*60; %[s]
Ti = 30; %[�C]
Tss = 70; %[�C]
PM = 18; %[g/mol]
DHev = 40.65/PM*1000; %[kJ/kg] <-- N.B. unit� di misura
ro = 1000; %[kg/m3]
F = V/t_res; %[m3/s]
cp = 4.186; %[kJ/(kg*K)]

Fvss = ro*F*cp*(Tss-Ti)/DHev; %[kg/s]

%Integrazione
hand = @(t,T)scalda(t,T,ro,F,cp,Ti,Fvss,DHev,V);
handout = @(t,T,flag)Fvout(t,T,flag,Fvss);
opt = odeset('OutputFcn',handout,'Refine',1,'AbsTol',1e-8,'RelTol',1e-12);
[t,T] = ode45(hand,[0 600],30,opt); %Tiniziale = 30�C

hold on
figure(1)
plot(t,T)
xlabel('t[s]')
ylabel('T[�C]')
title('Andamento temperatura')
figure(2)
plot(t,Fvvett,'r')
xlabel('t[s]')
ylabel('Fv[kg/s]')
title('Portata di vapore')

function Tp = scalda(~,T,ro,F,cp,Ti,Fv,DHev,V)

Tp = (ro*F*cp*(Ti-T) + Fv*DHev)/(ro*V*cp);

end

function status = Fvout(~,~,flag,Fvss)
global Fvvett
status = 0;
if strcmp(flag,'')
    Fv = Fvss;
    Fvvett = [Fvvett Fv]; % <-- N.B. scrivere il vettore cos�
elseif strcmp(flag,'init')
    Fvvett = Fvss;
end
end