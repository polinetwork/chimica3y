%% Risoluzione dell'esercitazione (4) di SECDIC
clc
clear all
close all
  
global Fin Fout D L Tin U ro cp Tamb Abase
  
% Dati:
D=8; % [m]
H=6; % [m]
Fin=10/60; % [m^3/s]
Tin=60; % [�C]
Fout=10/60; % [m^3/s]
L=2.5; % [m]
U=100; % [W/(m^2 K)]
ro=1000; % [kg/m^3]
cp=4186; % [J/(kg K)]
Tamb=20; % [�C]
Abase=pi*D^2/4; % [m^2]

% Ipotizziamo che lo scambio termico avvenga nella zona ricopeta dal
% liquido. Vogliamo determinare il calore Q da fornire al serbatoio per
% mantenere la sua temperatura costante.
  
% Risoluzione:
% Lavoriamo inizialmente ipotizzando delle condizioni stazionarie, poi
% vediamo tutti i punti successivi
  
% Caso 1): determinazione dello stato stazionario

options= odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.1:10000; % [s]
[t,T] = ode45('fEx1Es4',tSpan,Tin,options); % Ipotizziamo che la T iniziale del serbatoio sia quella di ingresso
lung=length(T);
Tstaz=T(lung);
disp(['Il valore assunto dalla T allo stato stazionario � ',num2str(Tstaz),' [�C]'])
figure('Name','Andamento di T nel caso stazionario','NumberTitle','off')
plot(t,T)
grid on
legend('T')
xlabel('Tempo [s]')
ylabel('Temperatura del serbatoio [�C]')
title('Andamento della T con il tempo')

% Determinazione del calore da fornire per mantenere la T inalterata, e
% pari a 60 [�C]

for i=1:length(T)
Q(i)=U*pi*D*L*(Tin-Tamb);
end
figure('Name','Andamento di Q nel caso di T stazionaria (1)','NumberTitle','off')
plot(t,Q)
grid on
legend('Q')
xlabel('Tempo [s]')
ylabel('Calore da fornire [W]')
title('Andamento della Q con il tempo (1)')
% Per andare a capo con Matlab si mettono 3 puntini (...) e si va a capo,
% continuando a scrivere il comando

% Caso 2): analisi dello stato non stazionario con Fout=0 per 10 minuti
Fout=0;
tSpan=0:0.1:10*60; % [s]
A0=[L Tstaz];
[t,A]=ode45('f2Ex1Es4',tSpan,A0,options); % Ipotizziamo che la T iniziale del serbatoio sia quella stazionaria
h=A(:,1)';
T=A(:,2)';
figure('Name','Andamento di h nel caso non stazionario, con Fin=10/60 e Fout=0','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Altezza del fluido nel serbatoio [m]')
title('Andamento della h con il tempo: Fin=10/60 e Fout=0')
figure('Name','Andamento di T nel caso non stazionario, con Fin=10/60 e Fout=0','NumberTitle','off')
plot(t,T)
grid on
legend('T')
xlabel('Tempo [s]')
ylabel('Temperatura del serbatoio [�C]')
title('Andamento della T con il tempo: Fin=10/60 e Fout=0')

% Determinazione del calore da fornire per mantenere la T inalterata, e
% pari a Tstaz


Q=U*pi*D.*h*(Tstaz-Tamb)-ro*Fin*cp*(Tin-Tstaz);


figure('Name','Andamento di Q nel caso di T stazionaria (2)','NumberTitle','off')
plot(t,Q)
grid on
legend('Q')
xlabel('Tempo [s]')
ylabel('Calore da fornire [W]')
title('Andamento della Q con il tempo (2)')

% Caso 3): analisi dello stato non stazionario con Fin=5/10 per 18 minuti
Fout=10/60; % [m^3/s]
Fin=5/60; % [m^3/s]
tSpan=0:0.1:18*60; % [s]
A0=[L Tstaz];
[t,A]=ode45('f2Ex1Es4',tSpan,A0,options); % Ipotizziamo che la T iniziale del serbatoio sia quella stazionaria
h=A(:,1)';
T=A(:,2)';
figure('Name','Andamento di h nel caso non stazionario, con Fin=5/60 e Fout=10/60','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Altezza del fluido nel serbatoio [m]')
title('Andamento della h con il tempo: Fin=10/60 e Fout=0')
figure('Name','Andamento di T nel caso non stazionario, con Fin=5/60 e Fout=10/60','NumberTitle','off')
plot(t,T)
grid on
legend('T')
xlabel('Tempo [s]')
ylabel('Temperatura del serbatoio [�C]')
title('Andamento della T con il tempo: Fin=10/60 e Fout=0')

% Determinazione del calore da fornire per mantenere la T inalterata, e
% pari a Tstaz

Q=U*pi*D.*h*(Tstaz-Tamb)-ro*Fin*cp*(Tin-Tstaz);

figure('Name','Andamento di Q nel caso di T stazionaria (3)','NumberTitle','off')
plot(t,Q)
grid on
legend('Q')
xlabel('Tempo [s]')
ylabel('Calore da fornire [W]')
title('Andamento della Q con il tempo (3)')
